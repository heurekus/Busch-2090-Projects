#!/usr/bin/python

#
# @version   1.0 2017-06-03
# @copyright Copyright (c) 2017 Martin Sauter, martin.sauter@wirelessmoves.com
# @license   GNU General Public License v2
# @since     Since Release 1.0
# 
#
# Overview
# ########
#
# This program sends the required signals to the Busch 2090 on 
# IN3, IN4 and IN2 to trigger PGM2 (save to tape) to output the
# program(s) stored in its memory on OUT1 with support signals 
# on OUT2 and OUT3.
#
# On the Raspberry Pi the following ports are set as OUTPUT and
# mapped as follows:
#
# GPIO21 -> IN3 (start of procedure)
# GPIO20 -> IN4 (start of 12 bit transmission (3x 4 bits)
# GPIO16 -> IN2 (clock out individual bits)
#
# Version 1 currently ONLY CREATES the signals to make the 
# Busch 2090 output. The program but DOES NOT read the bits yet
# (i.e. no GPIO set as input and used)
#
# IMPORTANT: 
#
# The Busch 2090 operates with 5V on its INs and OUTs while the 
# Rasbperry PI uses 3.3V!
# 
# RASPI OUTPUTS: Need to be level shifted from 3.3 to 5V. This
# was done with a HEF4081BP (4xAND) as 3.3 is interpreted as
# high and the output is put to 5V if VDD = 5V!
#
# RASPI INPUTS: Need to be protected! --> Not done yet, perhaps two
# resistors that split the 5V in 1/3 - 2/3 will do the trick.
# TBC.
#


import RPi.GPIO as GPIO # Import GPIO Library
import time,sys        


# Output ports
out_start = 21
out_4 = 20
out_2 = 16

# Delays
delay_high       = 0.001
delay_low_signal = 0.00015
delay_next_value = 0.17

# Use BOARD pin numbering
GPIO.setmode (GPIO.BCM) 

# set output ports
GPIO.setup (out_start, GPIO.OUT)  
GPIO.setup (out_4, GPIO.OUT)  
GPIO.setup (out_2, GPIO.OUT)  



def clockSignal (pin, delay):
  GPIO.output (pin, GPIO.HIGH)     
  time.sleep (delay)                
  GPIO.output (pin, GPIO.LOW)       
  #1.5 ms seems to be the lowest value the signal can be low
  time.sleep (delay_low_signal)
  return;

clockSignal (out_start, 0.1)

x = 0

while (x<10):
  try:

    # Start next 3 x 4 byte transmission
    clockSignal (out_4, delay_high)
    
    # Output one bit
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    clockSignal (out_2, delay_high)
    
    # Give the 2090 some time to get the next 12 bit value from RAM
    # 170 ms seems to be the lowest possible value...
    time.sleep(delay_next_value)
    print x
    x = x + 1
    
  except KeyboardInterrupt:
    GPIO.cleanup()
    sys.exit()

GPIO.cleanup()
