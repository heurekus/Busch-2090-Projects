TEMPLATE	= app
LANGUAGE	= C++

CONFIG	+= qt thread warn_on release

HEADERS	+= MIC2090.h \
	Nimm-Spiel.h \
	MICclock.h \
	Combos.h \
	convert.h \
	MyEvents.h \
	stdin_thread.h

SOURCES	+= main.cpp \
	MIC2090.cpp \
	MICclock.cpp \
	convert.cpp \
	stdin_thread.cpp

FORMS	= microtronic.ui

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}
