/****************************************************************************
** Form implementation generated from reading ui file 'microtronic.ui'
**
** Created: Sat Jul 8 07:46:51 2017
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "microtronic.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qbuttongroup.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qlcdnumber.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qaction.h>
#include <qmenubar.h>
#include <qpopupmenu.h>
#include <qtoolbar.h>
#include <qimage.h>
#include <qpixmap.h>

#include "../microtronic.ui.h"
static const unsigned char image0_data[] = { 
    0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
    0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x00, 0x00, 0x1e,
    0x08, 0x06, 0x00, 0x00, 0x00, 0x70, 0x98, 0x7d, 0x4f, 0x00, 0x00, 0x02,
    0xd3, 0x49, 0x44, 0x41, 0x54, 0x78, 0x9c, 0xdd, 0x98, 0xbf, 0x4e, 0xeb,
    0x30, 0x14, 0xc6, 0x7f, 0x45, 0x45, 0x8a, 0x90, 0x58, 0x32, 0x14, 0x81,
    0x22, 0x18, 0xf0, 0xc6, 0x0b, 0xa0, 0xf6, 0x35, 0xaa, 0xb0, 0x95, 0xf7,
    0x60, 0xb9, 0xb9, 0x0f, 0xd0, 0x07, 0x80, 0xad, 0xdd, 0x88, 0xf2, 0x06,
    0x77, 0x0e, 0xe2, 0x05, 0xba, 0xb9, 0x0b, 0x8a, 0x2e, 0xa2, 0x43, 0xc7,
    0xae, 0xb9, 0x03, 0x71, 0x70, 0x1c, 0x27, 0x69, 0x7a, 0xf9, 0x17, 0x3e,
    0x29, 0xb2, 0x4f, 0xce, 0x39, 0xae, 0x3f, 0x7d, 0xc7, 0x76, 0xdc, 0xde,
    0xcd, 0xcd, 0x4d, 0x7a, 0x75, 0x75, 0xc5, 0xfd, 0xfd, 0x3d, 0x00, 0x49,
    0x92, 0xf4, 0x3c, 0xcf, 0x4b, 0x93, 0x24, 0xe9, 0xf1, 0x03, 0xd1, 0x3f,
    0x3b, 0x3b, 0x63, 0x3a, 0x9d, 0xe6, 0xe4, 0x46, 0xa3, 0x51, 0x1a, 0xc7,
    0xf1, 0x8f, 0x24, 0x0b, 0xd0, 0xbf, 0xbc, 0xbc, 0x24, 0x8e, 0x63, 0xa4,
    0x94, 0x29, 0x80, 0x94, 0x12, 0x20, 0xfd, 0xda, 0x69, 0x7d, 0x0c, 0x84,
    0x10, 0xbd, 0xfe, 0xe3, 0xe3, 0xa3, 0xfe, 0xe2, 0x0b, 0xa7, 0xf3, 0xb1,
    0x88, 0xe3, 0x18, 0xc7, 0x71, 0xd2, 0x5c, 0x61, 0x05, 0x67, 0x3e, 0x07,
    0xe0, 0x18, 0x78, 0x36, 0xda, 0x2e, 0xe2, 0x37, 0x30, 0x99, 0x4c, 0x58,
    0x2e, 0x97, 0x2c, 0x97, 0x4b, 0xf6, 0xf4, 0xf5, 0xab, 0xe3, 0xd9, 0xd2,
    0x3e, 0xdb, 0x02, 0xbf, 0x39, 0x4e, 0x4f, 0x4f, 0x0b, 0xf6, 0x5e, 0x55,
    0xe0, 0xb1, 0xa5, 0xdf, 0x45, 0x95, 0x9f, 0x9e, 0x9e, 0x0a, 0x76, 0x25,
    0x61, 0xb3, 0x8c, 0xbb, 0x48, 0x16, 0x60, 0x38, 0x1c, 0x16, 0xec, 0xfe,
    0x36, 0x49, 0x7a, 0x29, 0x77, 0x95, 0xb8, 0x42, 0x89, 0xb0, 0xda, 0xa4,
    0x74, 0x1b, 0x60, 0x91, 0xb5, 0x6b, 0xe0, 0xc2, 0x32, 0xd0, 0x42, 0xeb,
    0xdb, 0xfc, 0x41, 0x18, 0x42, 0x18, 0xbe, 0xf6, 0xa3, 0xa8, 0xec, 0x1f,
    0x8f, 0xc1, 0xf7, 0x21, 0x0c, 0xab, 0xfd, 0xaa, 0x5f, 0xe7, 0xf7, 0x7d,
    0x02, 0xdf, 0xb7, 0xcc, 0xe0, 0x15, 0x95, 0x0a, 0xeb, 0x4a, 0x2e, 0x28,
    0x92, 0xd8, 0xc5, 0xd6, 0x27, 0x11, 0x84, 0x61, 0xd1, 0x1e, 0x8f, 0x5f,
    0x49, 0x64, 0x44, 0x72, 0xbb, 0x29, 0xde, 0xe6, 0x8f, 0xa2, 0x92, 0x5f,
    0x47, 0x69, 0x0d, 0x7f, 0xca, 0x11, 0x94, 0x29, 0x9d, 0xa3, 0x46, 0x91,
    0x9d, 0xe2, 0x6b, 0x50, 0x22, 0x6c, 0x92, 0x7d, 0x0f, 0x75, 0x75, 0xbb,
    0xa4, 0xce, 0x78, 0x5c, 0xaf, 0xde, 0x36, 0xf1, 0x66, 0xf5, 0x54, 0xa8,
    0x0b, 0xd0, 0x9b, 0x4c, 0x26, 0x00, 0x48, 0x29, 0x53, 0x21, 0x04, 0xce,
    0x7c, 0x6e, 0x25, 0xad, 0xb0, 0xd3, 0xfa, 0x6d, 0x58, 0x9f, 0x5b, 0xe5,
    0xab, 0x7e, 0xcb, 0xfc, 0x3f, 0xc3, 0x21, 0x42, 0x08, 0xe6, 0xd9, 0x07,
    0xd5, 0xd6, 0x0a, 0xab, 0x67, 0xd1, 0xd2, 0xaf, 0x14, 0x0a, 0x7c, 0x9f,
    0x20, 0x8a, 0xac, 0x7e, 0x3d, 0x5f, 0x27, 0xa7, 0xc6, 0x0f, 0xa2, 0xe8,
    0xed, 0x31, 0xca, 0xbb, 0x29, 0xdf, 0x44, 0xe5, 0x39, 0xac, 0x93, 0xd9,
    0xd5, 0x36, 0xcb, 0xb1, 0xe4, 0x37, 0x14, 0x6f, 0x8c, 0x6f, 0x59, 0xfe,
    0x36, 0x58, 0x37, 0xad, 0x4f, 0x83, 0xb9, 0x19, 0xb5, 0xc5, 0x0e, 0x9b,
    0x57, 0xe9, 0x58, 0xba, 0x03, 0x7e, 0x65, 0x7d, 0xb3, 0x44, 0xcd, 0xf5,
    0xd1, 0xe4, 0x0f, 0xb2, 0x23, 0x42, 0x4d, 0xce, 0x3c, 0x1f, 0xf3, 0x12,
    0xad, 0x38, 0x9f, 0x2f, 0xd0, 0xce, 0x6f, 0x5b, 0xbe, 0xef, 0x57, 0x8e,
    0x7f, 0x02, 0x70, 0x7b, 0x0b, 0xb3, 0x59, 0x21, 0xa7, 0xf1, 0x4b, 0xcb,
    0xb6, 0x89, 0xb4, 0xf1, 0xe7, 0x24, 0x2a, 0x4a, 0xad, 0xee, 0x23, 0xa1,
    0xe0, 0xaf, 0xca, 0xb7, 0x8c, 0x7f, 0x02, 0xfc, 0xcd, 0x5a, 0x13, 0xb5,
    0x6b, 0xb8, 0xab, 0x50, 0x64, 0xf5, 0x6b, 0xaf, 0x42, 0x2d, 0xe1, 0x2e,
    0x5e, 0x07, 0xe1, 0x4d, 0xe1, 0xd1, 0x68, 0x54, 0xf2, 0x59, 0x09, 0x77,
    0x95, 0xa8, 0xc2, 0xce, 0x0a, 0x77, 0xf1, 0x66, 0x74, 0x42, 0xbd, 0xc2,
    0xd6, 0x4d, 0xeb, 0xae, 0x66, 0xc0, 0xc1, 0x60, 0x00, 0xc0, 0x6a, 0xb5,
    0x2a, 0xf5, 0x57, 0xab, 0x55, 0x1e, 0xe3, 0x38, 0x4e, 0x29, 0xd7, 0xf3,
    0x3c, 0x92, 0x24, 0xd9, 0x7a, 0xf2, 0xb6, 0x78, 0xcf, 0xf3, 0x4a, 0x71,
    0xeb, 0xf5, 0x1a, 0x00, 0xd7, 0x75, 0xf3, 0x77, 0xd7, 0xd7, 0xd7, 0xd6,
    0x31, 0xb7, 0xba, 0x0f, 0xeb, 0xd0, 0x49, 0xa9, 0xbe, 0xf9, 0xfe, 0xf0,
    0xf0, 0xd0, 0x9a, 0xfb, 0xf2, 0xf2, 0xc2, 0xfe, 0xfe, 0xfe, 0xd6, 0xbf,
    0x65, 0x8b, 0x57, 0xe4, 0x6c, 0xb0, 0x11, 0x37, 0xd1, 0x9a, 0xb0, 0x82,
    0x4e, 0x76, 0x30, 0x18, 0x20, 0x84, 0x40, 0x4a, 0x89, 0x10, 0x82, 0xcd,
    0x66, 0xb3, 0xeb, 0xb0, 0xef, 0x82, 0xcd, 0x66, 0xc3, 0x6c, 0x36, 0xb3,
    0xaa, 0xdc, 0x9a, 0xf0, 0xf9, 0xf9, 0x39, 0x47, 0x47, 0x47, 0x39, 0x39,
    0xb3, 0x05, 0x38, 0x38, 0x38, 0xf8, 0xff, 0x59, 0xbf, 0x03, 0x66, 0xc6,
    0x47, 0x07, 0x58, 0x08, 0xab, 0xdb, 0x53, 0x13, 0x14, 0x39, 0xb3, 0xfd,
    0xee, 0xc8, 0x09, 0xbb, 0xae, 0x8b, 0x94, 0x92, 0x87, 0x87, 0x87, 0xaf,
    0x9c, 0xcf, 0x87, 0x23, 0x3f, 0x96, 0x5c, 0xd7, 0xed, 0x41, 0xf9, 0x5f,
    0xbe, 0x9f, 0x86, 0x7f, 0xb2, 0x7a, 0x53, 0xb4, 0xb5, 0xcf, 0x5e, 0xa1,
    0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82
};


/*
 *  Constructs a mainform as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 */
mainform::mainform( QWidget* parent, const char* name, WFlags fl )
    : QMainWindow( parent, name, fl )
{
    (void)statusBar();
    QImage img;
    img.loadFromData( image0_data, sizeof( image0_data ), "PNG" );
    image0 = img;
    if ( !name )
	setName( "mainform" );
    setIcon( image0 );
    setCentralWidget( new QWidget( this, "qt_central_widget" ) );
    mainformLayout = new QVBoxLayout( centralWidget(), 11, 6, "mainformLayout"); 

    layout8 = new QHBoxLayout( 0, 0, 6, "layout8"); 

    Output = new QButtonGroup( centralWidget(), "Output" );
    Output->setColumnLayout(0, Qt::Vertical );
    Output->layout()->setSpacing( 6 );
    Output->layout()->setMargin( 11 );
    OutputLayout = new QHBoxLayout( Output->layout() );
    OutputLayout->setAlignment( Qt::AlignTop );

    out1 = new QCheckBox( Output, "out1" );
    out1->setEnabled( FALSE );
    out1->setPaletteForegroundColor( QColor( 0, 0, 0 ) );
    out1->setFocusPolicy( QCheckBox::NoFocus );
    Output->insert( out1, 1 );
    OutputLayout->addWidget( out1 );

    out2 = new QCheckBox( Output, "out2" );
    out2->setEnabled( FALSE );
    out2->setPaletteForegroundColor( QColor( 0, 0, 0 ) );
    out2->setFocusPolicy( QCheckBox::NoFocus );
    Output->insert( out2, 2 );
    OutputLayout->addWidget( out2 );

    out3 = new QCheckBox( Output, "out3" );
    out3->setEnabled( FALSE );
    out3->setPaletteForegroundColor( QColor( 0, 0, 0 ) );
    out3->setFocusPolicy( QCheckBox::NoFocus );
    Output->insert( out3, 3 );
    OutputLayout->addWidget( out3 );

    out4 = new QCheckBox( Output, "out4" );
    out4->setEnabled( FALSE );
    out4->setPaletteForegroundColor( QColor( 0, 0, 0 ) );
    out4->setFocusPolicy( QCheckBox::NoFocus );
    Output->insert( out4, 4 );
    OutputLayout->addWidget( out4 );
    layout8->addWidget( Output );

    Input = new QButtonGroup( centralWidget(), "Input" );
    Input->setColumnLayout(0, Qt::Vertical );
    Input->layout()->setSpacing( 6 );
    Input->layout()->setMargin( 11 );
    InputLayout = new QHBoxLayout( Input->layout() );
    InputLayout->setAlignment( Qt::AlignTop );

    in1 = new QCheckBox( Input, "in1" );
    Input->insert( in1, 1 );
    InputLayout->addWidget( in1 );

    in2 = new QCheckBox( Input, "in2" );
    Input->insert( in2, 2 );
    InputLayout->addWidget( in2 );

    in3 = new QCheckBox( Input, "in3" );
    Input->insert( in3, 3 );
    InputLayout->addWidget( in3 );

    in4 = new QCheckBox( Input, "in4" );
    Input->insert( in4, 4 );
    InputLayout->addWidget( in4 );
    layout8->addWidget( Input );

    Clock = new QButtonGroup( centralWidget(), "Clock" );
    Clock->setAlignment( int( QButtonGroup::AlignHCenter ) );
    Clock->setColumnLayout(0, Qt::Vertical );
    Clock->layout()->setSpacing( 0 );
    Clock->layout()->setMargin( 11 );
    ClockLayout = new QHBoxLayout( Clock->layout() );
    ClockLayout->setAlignment( Qt::AlignTop );
    spacer2 = new QSpacerItem( 1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum );
    ClockLayout->addItem( spacer2 );

    clock = new QCheckBox( Clock, "clock" );
    clock->setEnabled( FALSE );
    clock->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)4, 0, 0, clock->sizePolicy().hasHeightForWidth() ) );
    clock->setMinimumSize( QSize( 20, 20 ) );
    clock->setPaletteForegroundColor( QColor( 0, 0, 0 ) );
    clock->setFocusPolicy( QCheckBox::NoFocus );
    ClockLayout->addWidget( clock );
    spacer3 = new QSpacerItem( 1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum );
    ClockLayout->addItem( spacer3 );
    layout8->addWidget( Clock );
    spacer9 = new QSpacerItem( 24, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout8->addItem( spacer9 );

    reset = new QButtonGroup( centralWidget(), "reset" );
    reset->setAlignment( int( QButtonGroup::AlignHCenter ) );
    reset->setColumnLayout(0, Qt::Vertical );
    reset->layout()->setSpacing( 0 );
    reset->layout()->setMargin( 11 );
    resetLayout = new QHBoxLayout( reset->layout() );
    resetLayout->setAlignment( Qt::AlignTop );
    spacer5 = new QSpacerItem( 1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum );
    resetLayout->addItem( spacer5 );

    resetkey = new QPushButton( reset, "resetkey" );
    resetkey->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, resetkey->sizePolicy().hasHeightForWidth() ) );
    resetkey->setMinimumSize( QSize( 20, 20 ) );
    resetkey->setMaximumSize( QSize( 20, 20 ) );
    resetkey->setPaletteBackgroundColor( QColor( 0, 192, 0 ) );
    QPalette pal;
    QColorGroup cg;
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 0, 192, 0) );
    cg.setColor( QColorGroup::Light, QColor( 33, 255, 33) );
    cg.setColor( QColorGroup::Midlight, QColor( 16, 223, 16) );
    cg.setColor( QColorGroup::Dark, QColor( 0, 96, 0) );
    cg.setColor( QColorGroup::Mid, QColor( 0, 128, 0) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 192, 192, 192) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, black );
    cg.setColor( QColorGroup::LinkVisited, black );
    pal.setActive( cg );
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 0, 192, 0) );
    cg.setColor( QColorGroup::Light, QColor( 33, 255, 33) );
    cg.setColor( QColorGroup::Midlight, QColor( 0, 220, 0) );
    cg.setColor( QColorGroup::Dark, QColor( 0, 96, 0) );
    cg.setColor( QColorGroup::Mid, QColor( 0, 128, 0) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 192, 192, 192) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setInactive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Button, QColor( 0, 192, 0) );
    cg.setColor( QColorGroup::Light, QColor( 33, 255, 33) );
    cg.setColor( QColorGroup::Midlight, QColor( 0, 220, 0) );
    cg.setColor( QColorGroup::Dark, QColor( 0, 96, 0) );
    cg.setColor( QColorGroup::Mid, QColor( 0, 128, 0) );
    cg.setColor( QColorGroup::Text, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 192, 192, 192) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setDisabled( cg );
    resetkey->setPalette( pal );
    resetLayout->addWidget( resetkey );
    spacer6 = new QSpacerItem( 1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum );
    resetLayout->addItem( spacer6 );
    layout8->addWidget( reset );
    spacer10 = new QSpacerItem( 26, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout8->addItem( spacer10 );

    off = new QButtonGroup( centralWidget(), "off" );
    off->setAlignment( int( QButtonGroup::AlignHCenter ) );
    off->setColumnLayout(0, Qt::Vertical );
    off->layout()->setSpacing( 0 );
    off->layout()->setMargin( 11 );
    offLayout = new QHBoxLayout( off->layout() );
    offLayout->setAlignment( Qt::AlignTop );
    spacer7 = new QSpacerItem( 1, 21, QSizePolicy::Minimum, QSizePolicy::Minimum );
    offLayout->addItem( spacer7 );

    offkey = new QPushButton( off, "offkey" );
    offkey->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, offkey->sizePolicy().hasHeightForWidth() ) );
    offkey->setMinimumSize( QSize( 20, 20 ) );
    offkey->setMaximumSize( QSize( 20, 20 ) );
    offkey->setPaletteBackgroundColor( QColor( 192, 0, 0 ) );
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 192, 0, 0) );
    cg.setColor( QColorGroup::Light, QColor( 255, 127, 127) );
    cg.setColor( QColorGroup::Midlight, QColor( 255, 63, 63) );
    cg.setColor( QColorGroup::Dark, QColor( 127, 0, 0) );
    cg.setColor( QColorGroup::Mid, QColor( 170, 0, 0) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 192, 192, 192) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, black );
    cg.setColor( QColorGroup::LinkVisited, black );
    pal.setActive( cg );
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 192, 0, 0) );
    cg.setColor( QColorGroup::Light, QColor( 255, 127, 127) );
    cg.setColor( QColorGroup::Midlight, QColor( 255, 38, 38) );
    cg.setColor( QColorGroup::Dark, QColor( 127, 0, 0) );
    cg.setColor( QColorGroup::Mid, QColor( 170, 0, 0) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 192, 192, 192) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setInactive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Button, QColor( 192, 0, 0) );
    cg.setColor( QColorGroup::Light, QColor( 255, 127, 127) );
    cg.setColor( QColorGroup::Midlight, QColor( 255, 38, 38) );
    cg.setColor( QColorGroup::Dark, QColor( 127, 0, 0) );
    cg.setColor( QColorGroup::Mid, QColor( 170, 0, 0) );
    cg.setColor( QColorGroup::Text, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 192, 192, 192) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setDisabled( cg );
    offkey->setPalette( pal );
    offLayout->addWidget( offkey );
    spacer8 = new QSpacerItem( 1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum );
    offLayout->addItem( spacer8 );
    layout8->addWidget( off );
    mainformLayout->addLayout( layout8 );

    EmuSettings = new QGroupBox( centralWidget(), "EmuSettings" );
    EmuSettings->setColumnLayout(0, Qt::Vertical );
    EmuSettings->layout()->setSpacing( 6 );
    EmuSettings->layout()->setMargin( 11 );
    EmuSettingsLayout = new QHBoxLayout( EmuSettings->layout() );
    EmuSettingsLayout->setAlignment( Qt::AlignTop );

    layout10 = new QGridLayout( 0, 1, 1, 0, 10, "layout10"); 

    InCon = new QComboBox( FALSE, EmuSettings, "InCon" );
    InCon->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, InCon->sizePolicy().hasHeightForWidth() ) );
    InCon->setMinimumSize( QSize( 140, 20 ) );
    InCon->setFocusPolicy( QComboBox::NoFocus );

    layout10->addWidget( InCon, 1, 1 );

    OutCon = new QComboBox( FALSE, EmuSettings, "OutCon" );
    OutCon->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, OutCon->sizePolicy().hasHeightForWidth() ) );
    OutCon->setMinimumSize( QSize( 140, 20 ) );
    OutCon->setFocusPolicy( QComboBox::NoFocus );
    OutCon->setEditable( FALSE );
    OutCon->setMaxCount( 3 );

    layout10->addWidget( OutCon, 1, 0 );

    text_incon = new QLabel( EmuSettings, "text_incon" );
    text_incon->setMargin( 1 );

    layout10->addWidget( text_incon, 0, 1 );

    text_outcon = new QLabel( EmuSettings, "text_outcon" );
    text_outcon->setMargin( 1 );

    layout10->addWidget( text_outcon, 0, 0 );
    EmuSettingsLayout->addLayout( layout10 );
    spacer17 = new QSpacerItem( 10, 20, QSizePolicy::Minimum, QSizePolicy::Minimum );
    EmuSettingsLayout->addItem( spacer17 );

    layout11 = new QVBoxLayout( 0, 0, 6, "layout11"); 

    maxspeed = new QCheckBox( EmuSettings, "maxspeed" );
    layout11->addWidget( maxspeed );

    regdump = new QCheckBox( EmuSettings, "regdump" );
    layout11->addWidget( regdump );
    EmuSettingsLayout->addLayout( layout11 );
    spacer11 = new QSpacerItem( 70, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    EmuSettingsLayout->addItem( spacer11 );
    mainformLayout->addWidget( EmuSettings );

    layout8_2 = new QHBoxLayout( 0, 0, 6, "layout8_2"); 

    layout51 = new QVBoxLayout( 0, 0, 6, "layout51"); 

    layout50 = new QHBoxLayout( 0, 0, 6, "layout50"); 

    flags = new QButtonGroup( centralWidget(), "flags" );
    flags->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)5, 0, 0, flags->sizePolicy().hasHeightForWidth() ) );
    flags->setColumnLayout(0, Qt::Vertical );
    flags->layout()->setSpacing( 6 );
    flags->layout()->setMargin( 11 );
    flagsLayout = new QVBoxLayout( flags->layout() );
    flagsLayout->setAlignment( Qt::AlignTop );

    carry = new QCheckBox( flags, "carry" );
    carry->setEnabled( FALSE );
    carry->setPaletteForegroundColor( QColor( 0, 0, 0 ) );
    carry->setFocusPolicy( QCheckBox::NoFocus );
    flagsLayout->addWidget( carry );

    zero = new QCheckBox( flags, "zero" );
    zero->setEnabled( FALSE );
    zero->setPaletteForegroundColor( QColor( 0, 0, 0 ) );
    zero->setFocusPolicy( QCheckBox::NoFocus );
    flagsLayout->addWidget( zero );
    spacer12 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding );
    flagsLayout->addItem( spacer12 );
    layout50->addWidget( flags );

    display = new QGroupBox( centralWidget(), "display" );
    display->setFlat( FALSE );
    display->setColumnLayout(0, Qt::Vertical );
    display->layout()->setSpacing( 6 );
    display->layout()->setMargin( 11 );
    displayLayout = new QGridLayout( display->layout() );
    displayLayout->setAlignment( Qt::AlignTop );

    layout49 = new QHBoxLayout( 0, 0, 0, "layout49"); 

    digit1 = new QLCDNumber( display, "digit1" );
    digit1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, digit1->sizePolicy().hasHeightForWidth() ) );
    digit1->setMinimumSize( QSize( 20, 30 ) );
    digit1->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    digit1->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    digit1->setFrameShape( QLCDNumber::NoFrame );
    digit1->setFrameShadow( QLCDNumber::Sunken );
    digit1->setNumDigits( 1 );
    digit1->setMode( QLCDNumber::Hex );
    digit1->setSegmentStyle( QLCDNumber::Flat );
    layout49->addWidget( digit1 );

    digit2 = new QLCDNumber( display, "digit2" );
    digit2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, digit2->sizePolicy().hasHeightForWidth() ) );
    digit2->setMinimumSize( QSize( 20, 30 ) );
    digit2->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    digit2->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    digit2->setFrameShape( QLCDNumber::NoFrame );
    digit2->setFrameShadow( QLCDNumber::Sunken );
    digit2->setNumDigits( 1 );
    digit2->setMode( QLCDNumber::Hex );
    digit2->setSegmentStyle( QLCDNumber::Flat );
    layout49->addWidget( digit2 );

    digit3 = new QLCDNumber( display, "digit3" );
    digit3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, digit3->sizePolicy().hasHeightForWidth() ) );
    digit3->setMinimumSize( QSize( 20, 30 ) );
    digit3->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    digit3->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    digit3->setFrameShape( QLCDNumber::NoFrame );
    digit3->setFrameShadow( QLCDNumber::Sunken );
    digit3->setNumDigits( 1 );
    digit3->setMode( QLCDNumber::Hex );
    digit3->setSegmentStyle( QLCDNumber::Flat );
    layout49->addWidget( digit3 );

    digit4 = new QLCDNumber( display, "digit4" );
    digit4->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, digit4->sizePolicy().hasHeightForWidth() ) );
    digit4->setMinimumSize( QSize( 20, 30 ) );
    digit4->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    digit4->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    digit4->setFrameShape( QLCDNumber::NoFrame );
    digit4->setFrameShadow( QLCDNumber::Sunken );
    digit4->setNumDigits( 1 );
    digit4->setMode( QLCDNumber::Hex );
    digit4->setSegmentStyle( QLCDNumber::Flat );
    layout49->addWidget( digit4 );

    digit5 = new QLCDNumber( display, "digit5" );
    digit5->setEnabled( TRUE );
    digit5->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, digit5->sizePolicy().hasHeightForWidth() ) );
    digit5->setMinimumSize( QSize( 20, 30 ) );
    digit5->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    digit5->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    digit5->setFrameShape( QLCDNumber::NoFrame );
    digit5->setFrameShadow( QLCDNumber::Sunken );
    digit5->setNumDigits( 1 );
    digit5->setMode( QLCDNumber::Hex );
    digit5->setSegmentStyle( QLCDNumber::Flat );
    layout49->addWidget( digit5 );

    digit6 = new QLCDNumber( display, "digit6" );
    digit6->setEnabled( FALSE );
    digit6->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, digit6->sizePolicy().hasHeightForWidth() ) );
    digit6->setMinimumSize( QSize( 20, 30 ) );
    digit6->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    digit6->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    digit6->setFrameShape( QLCDNumber::NoFrame );
    digit6->setFrameShadow( QLCDNumber::Sunken );
    digit6->setNumDigits( 1 );
    digit6->setMode( QLCDNumber::Hex );
    digit6->setSegmentStyle( QLCDNumber::Flat );
    layout49->addWidget( digit6 );

    displayLayout->addLayout( layout49, 0, 0 );
    layout50->addWidget( display );
    layout51->addLayout( layout50 );

    microtronic = new QLabel( centralWidget(), "microtronic" );
    microtronic->setMinimumSize( QSize( 0, 30 ) );
    microtronic->setMaximumSize( QSize( 32767, 30 ) );
    microtronic->setPaletteBackgroundColor( QColor( 235, 232, 224 ) );
    QFont microtronic_font(  microtronic->font() );
    microtronic_font.setFamily( "Helvetica [Adobe]" );
    microtronic_font.setPointSize( 20 );
    microtronic_font.setItalic( TRUE );
    microtronic->setFont( microtronic_font ); 
    microtronic->setFrameShape( QLabel::Box );
    microtronic->setFrameShadow( QLabel::Sunken );
    microtronic->setScaledContents( TRUE );
    layout51->addWidget( microtronic );
    layout8_2->addLayout( layout51 );

    HexKeys = new QButtonGroup( centralWidget(), "HexKeys" );
    HexKeys->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, HexKeys->sizePolicy().hasHeightForWidth() ) );
    HexKeys->setColumnLayout(0, Qt::Vertical );
    HexKeys->layout()->setSpacing( 6 );
    HexKeys->layout()->setMargin( 11 );
    HexKeysLayout = new QGridLayout( HexKeys->layout() );
    HexKeysLayout->setAlignment( Qt::AlignTop );

    key1 = new QPushButton( HexKeys, "key1" );
    key1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key1->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key1, 1 );

    HexKeysLayout->addWidget( key1, 3, 1 );

    key2 = new QPushButton( HexKeys, "key2" );
    key2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key2->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key2, 2 );

    HexKeysLayout->addWidget( key2, 3, 2 );

    key3 = new QPushButton( HexKeys, "key3" );
    key3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key3->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key3, 3 );

    HexKeysLayout->addWidget( key3, 3, 3 );

    key4 = new QPushButton( HexKeys, "key4" );
    key4->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key4->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key4, 4 );

    HexKeysLayout->addWidget( key4, 2, 0 );

    key5 = new QPushButton( HexKeys, "key5" );
    key5->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key5->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key5, 5 );

    HexKeysLayout->addWidget( key5, 2, 1 );

    key6 = new QPushButton( HexKeys, "key6" );
    key6->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key6->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key6, 6 );

    HexKeysLayout->addWidget( key6, 2, 2 );

    key7 = new QPushButton( HexKeys, "key7" );
    key7->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key7->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key7, 7 );

    HexKeysLayout->addWidget( key7, 2, 3 );

    key8 = new QPushButton( HexKeys, "key8" );
    key8->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key8->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key8, 8 );

    HexKeysLayout->addWidget( key8, 1, 0 );

    key9 = new QPushButton( HexKeys, "key9" );
    key9->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key9->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key9, 9 );

    HexKeysLayout->addWidget( key9, 1, 1 );

    keyB = new QPushButton( HexKeys, "keyB" );
    keyB->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, keyB->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( keyB, 11 );

    HexKeysLayout->addWidget( keyB, 1, 3 );

    keyC = new QPushButton( HexKeys, "keyC" );
    keyC->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, keyC->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( keyC, 12 );

    HexKeysLayout->addWidget( keyC, 0, 0 );

    keyD = new QPushButton( HexKeys, "keyD" );
    keyD->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, keyD->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( keyD, 13 );

    HexKeysLayout->addWidget( keyD, 0, 1 );

    keyE = new QPushButton( HexKeys, "keyE" );
    keyE->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, keyE->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( keyE, 14 );

    HexKeysLayout->addWidget( keyE, 0, 2 );

    keyA = new QPushButton( HexKeys, "keyA" );
    keyA->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, keyA->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( keyA, 10 );

    HexKeysLayout->addWidget( keyA, 1, 2 );

    keyF = new QPushButton( HexKeys, "keyF" );
    keyF->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, keyF->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( keyF, 15 );

    HexKeysLayout->addWidget( keyF, 0, 3 );

    key0 = new QPushButton( HexKeys, "key0" );
    key0->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, key0->sizePolicy().hasHeightForWidth() ) );
    HexKeys->insert( key0, 0 );

    HexKeysLayout->addWidget( key0, 3, 0 );
    layout8_2->addWidget( HexKeys );

    ControlKeys = new QButtonGroup( centralWidget(), "ControlKeys" );
    ControlKeys->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)1, 0, 0, ControlKeys->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->setAlignment( int( QButtonGroup::AlignCenter ) );
    ControlKeys->setColumnLayout(0, Qt::Vertical );
    ControlKeys->layout()->setSpacing( 6 );
    ControlKeys->layout()->setMargin( 11 );
    ControlKeysLayout = new QGridLayout( ControlKeys->layout() );
    ControlKeysLayout->setAlignment( Qt::AlignTop );

    pgm_key = new QPushButton( ControlKeys, "pgm_key" );
    pgm_key->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)7, 0, 0, pgm_key->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->insert( pgm_key, 1 );

    ControlKeysLayout->addWidget( pgm_key, 3, 1 );

    run_key = new QPushButton( ControlKeys, "run_key" );
    run_key->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)7, 0, 0, run_key->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->insert( run_key, 2 );

    ControlKeysLayout->addWidget( run_key, 2, 0 );

    halt_key = new QPushButton( ControlKeys, "halt_key" );
    halt_key->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)7, 0, 0, halt_key->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->insert( halt_key, 3 );

    ControlKeysLayout->addWidget( halt_key, 2, 1 );

    bkp_key = new QPushButton( ControlKeys, "bkp_key" );
    bkp_key->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)7, 0, 0, bkp_key->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->insert( bkp_key, 4 );

    ControlKeysLayout->addWidget( bkp_key, 1, 0 );

    step_key = new QPushButton( ControlKeys, "step_key" );
    step_key->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)7, 0, 0, step_key->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->insert( step_key, 5 );

    ControlKeysLayout->addWidget( step_key, 1, 1 );

    next_key = new QPushButton( ControlKeys, "next_key" );
    next_key->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)7, 0, 0, next_key->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->insert( next_key, 6 );

    ControlKeysLayout->addWidget( next_key, 0, 0 );

    reg_key = new QPushButton( ControlKeys, "reg_key" );
    reg_key->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)7, 0, 0, reg_key->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->insert( reg_key, 7 );

    ControlKeysLayout->addWidget( reg_key, 0, 1 );

    cce_key = new QPushButton( ControlKeys, "cce_key" );
    cce_key->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)7, 0, 0, cce_key->sizePolicy().hasHeightForWidth() ) );
    ControlKeys->insert( cce_key, 0 );

    ControlKeysLayout->addWidget( cce_key, 3, 0 );
    layout8_2->addWidget( ControlKeys );
    mainformLayout->addLayout( layout8_2 );

    // toolbars

    languageChange();
    resize( QSize(585, 323).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( offkey, SIGNAL( pressed() ), this, SLOT( reset_key_pressed() ) );
    connect( resetkey, SIGNAL( pressed() ), this, SLOT( reset_key_pressed() ) );
    connect( resetkey, SIGNAL( released() ), this, SLOT( reset_key_released() ) );
    connect( HexKeys, SIGNAL( released(int) ), this, SLOT( HexKey_released(int) ) );
    connect( ControlKeys, SIGNAL( released(int) ), this, SLOT( CKey_released(int) ) );
    connect( in1, SIGNAL( toggled(bool) ), this, SLOT( in1_toggled(bool) ) );
    connect( in2, SIGNAL( toggled(bool) ), this, SLOT( in2_toggled(bool) ) );
    connect( in3, SIGNAL( toggled(bool) ), this, SLOT( in3_toggled(bool) ) );
    connect( in4, SIGNAL( toggled(bool) ), this, SLOT( in4_toggled(bool) ) );
    connect( maxspeed, SIGNAL( toggled(bool) ), this, SLOT( maxspeed_toggled(bool) ) );
    connect( InCon, SIGNAL( activated(int) ), this, SLOT( InConChanged() ) );
    connect( regdump, SIGNAL( toggled(bool) ), this, SLOT( regdump_toggled(bool) ) );
    connect( offkey, SIGNAL( released() ), this, SLOT( close() ) );
    connect( out1, SIGNAL( toggled(bool) ), this, SLOT( out1_toggled(bool) ) );
    connect( out2, SIGNAL( toggled(bool) ), this, SLOT( out2_toggled(bool) ) );
    connect( out3, SIGNAL( toggled(bool) ), this, SLOT( out3_toggled(bool) ) );
    connect( out4, SIGNAL( toggled(bool) ), this, SLOT( out4_toggled(bool) ) );

    // tab order
    setTabOrder( in1, in2 );
    setTabOrder( in2, in3 );
    setTabOrder( in3, in4 );
    setTabOrder( in4, maxspeed );
    setTabOrder( maxspeed, regdump );
    setTabOrder( regdump, resetkey );
    setTabOrder( resetkey, offkey );
    setTabOrder( offkey, key0 );
    setTabOrder( key0, key1 );
    setTabOrder( key1, key2 );
    setTabOrder( key2, key3 );
    setTabOrder( key3, key4 );
    setTabOrder( key4, key5 );
    setTabOrder( key5, key6 );
    setTabOrder( key6, key7 );
    setTabOrder( key7, key8 );
    setTabOrder( key8, key9 );
    setTabOrder( key9, keyA );
    setTabOrder( keyA, keyB );
    setTabOrder( keyB, keyC );
    setTabOrder( keyC, keyD );
    setTabOrder( keyD, keyE );
    setTabOrder( keyE, keyF );
    setTabOrder( keyF, next_key );
    setTabOrder( next_key, reg_key );
    setTabOrder( reg_key, bkp_key );
    setTabOrder( bkp_key, step_key );
    setTabOrder( step_key, run_key );
    setTabOrder( run_key, halt_key );
    setTabOrder( halt_key, cce_key );
    setTabOrder( cce_key, pgm_key );
    setTabOrder( pgm_key, clock );
    setTabOrder( clock, carry );
    setTabOrder( carry, zero );
}

/*
 *  Destroys the object and frees any allocated resources
 */
mainform::~mainform()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void mainform::languageChange()
{
    setCaption( tr( "Microtronic 2090" ) );
    setIconText( tr( "2090" ) );
    Output->setTitle( tr( "Output" ) );
    out1->setText( tr( "1" ) );
    out2->setText( tr( "2" ) );
    out3->setText( tr( "3" ) );
    out4->setText( tr( "4" ) );
    Input->setTitle( tr( "Input" ) );
    in1->setText( tr( "1" ) );
    in2->setText( tr( "2" ) );
    in3->setText( tr( "3" ) );
    in4->setText( tr( "4" ) );
    Clock->setTitle( tr( "Clock" ) );
    clock->setText( QString::null );
    reset->setTitle( tr( "Reset" ) );
    resetkey->setText( QString::null );
    off->setTitle( tr( "Off" ) );
    offkey->setText( QString::null );
    EmuSettings->setTitle( tr( "Emulator settings" ) );
    InCon->clear();
    InCon->insertItem( tr( "none" ) );
    InCon->insertItem( tr( "stdin" ) );
    InCon->insertItem( tr( "clock on 1" ) );
    InCon->insertItem( tr( "clock on 2" ) );
    InCon->insertItem( tr( "clock on 3" ) );
    InCon->insertItem( tr( "clock on 4" ) );
    InCon->insertItem( tr( "outputs to inputs" ) );
    InCon->insertItem( tr( "F1...4 on 1...4" ) );
    InCon->insertItem( tr( "F1...3,clock on 1...4" ) );
    InCon->insertItem( tr( "clk,F1,F2,stdin on 1...4" ) );
    OutCon->clear();
    OutCon->insertItem( tr( "none" ) );
    OutCon->insertItem( tr( "stdout" ) );
    OutCon->insertItem( tr( "buzzer on 1" ) );
    OutCon->insertItem( tr( "buzzer on 2" ) );
    OutCon->insertItem( tr( "buzzer on 3" ) );
    OutCon->insertItem( tr( "buzzer on 4" ) );
    text_incon->setText( tr( "Input connexion" ) );
    text_outcon->setText( tr( "Output connexion" ) );
    maxspeed->setText( tr( "Maximum speed" ) );
    regdump->setText( tr( "Register dump" ) );
    flags->setTitle( tr( "Flags" ) );
    carry->setText( tr( "Carry" ) );
    zero->setText( tr( "Zero" ) );
    display->setTitle( tr( "Display" ) );
    microtronic->setText( tr( "microtronic" ) );
    HexKeys->setTitle( QString::null );
    key1->setText( tr( "1" ) );
    key1->setAccel( QKeySequence( tr( "1" ) ) );
    key2->setText( tr( "2" ) );
    key2->setAccel( QKeySequence( tr( "2" ) ) );
    key3->setText( tr( "3" ) );
    key3->setAccel( QKeySequence( tr( "3" ) ) );
    key4->setText( tr( "4" ) );
    key4->setAccel( QKeySequence( tr( "4" ) ) );
    key5->setText( tr( "5" ) );
    key5->setAccel( QKeySequence( tr( "5" ) ) );
    key6->setText( tr( "6" ) );
    key6->setAccel( QKeySequence( tr( "6" ) ) );
    key7->setText( tr( "7" ) );
    key7->setAccel( QKeySequence( tr( "7" ) ) );
    key8->setText( tr( "8" ) );
    key8->setAccel( QKeySequence( tr( "8" ) ) );
    key9->setText( tr( "9" ) );
    key9->setAccel( QKeySequence( tr( "9" ) ) );
    keyB->setText( tr( "B" ) );
    keyB->setAccel( QKeySequence( tr( "B" ) ) );
    keyC->setText( tr( "C" ) );
    keyC->setAccel( QKeySequence( tr( "C" ) ) );
    keyD->setText( tr( "D" ) );
    keyD->setAccel( QKeySequence( tr( "D" ) ) );
    keyE->setText( tr( "E" ) );
    keyE->setAccel( QKeySequence( tr( "E" ) ) );
    keyA->setText( tr( "A" ) );
    keyA->setAccel( QKeySequence( tr( "A" ) ) );
    keyF->setText( tr( "F" ) );
    keyF->setAccel( QKeySequence( tr( "F" ) ) );
    key0->setText( tr( "0" ) );
    key0->setAccel( QKeySequence( tr( "0" ) ) );
    ControlKeys->setTitle( QString::null );
    pgm_key->setText( tr( "PGM" ) );
    pgm_key->setAccel( QKeySequence( tr( "P" ) ) );
    run_key->setText( tr( "RUN" ) );
    run_key->setAccel( QKeySequence( tr( "R" ) ) );
    halt_key->setText( tr( "HALT" ) );
    halt_key->setAccel( QKeySequence( tr( "Esc" ) ) );
    bkp_key->setText( tr( "BKP" ) );
    bkp_key->setAccel( QKeySequence( tr( "Ins" ) ) );
    step_key->setText( tr( "STEP" ) );
    step_key->setAccel( QKeySequence( tr( "S" ) ) );
    next_key->setText( tr( "NEXT" ) );
    next_key->setAccel( QKeySequence( tr( "Return" ) ) );
    reg_key->setText( tr( "REG" ) );
    reg_key->setAccel( QKeySequence( tr( "G" ) ) );
    cce_key->setText( tr( "C/CE" ) );
    cce_key->setAccel( QKeySequence( tr( "Del" ) ) );
}

