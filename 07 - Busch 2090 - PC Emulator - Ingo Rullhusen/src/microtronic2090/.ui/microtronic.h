/****************************************************************************
** Form interface generated from reading ui file 'microtronic.ui'
**
** Created: Sat Jul 8 07:46:48 2017
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef MAINFORM_H
#define MAINFORM_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qmainwindow.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QAction;
class QActionGroup;
class QToolBar;
class QPopupMenu;
class QButtonGroup;
class QCheckBox;
class QPushButton;
class QGroupBox;
class QComboBox;
class QLabel;
class QLCDNumber;

class mainform : public QMainWindow
{
    Q_OBJECT

public:
    mainform( QWidget* parent = 0, const char* name = 0, WFlags fl = WType_TopLevel );
    ~mainform();

    QButtonGroup* Output;
    QCheckBox* out1;
    QCheckBox* out2;
    QCheckBox* out3;
    QCheckBox* out4;
    QButtonGroup* Input;
    QCheckBox* in1;
    QCheckBox* in2;
    QCheckBox* in3;
    QCheckBox* in4;
    QButtonGroup* Clock;
    QCheckBox* clock;
    QButtonGroup* reset;
    QPushButton* resetkey;
    QButtonGroup* off;
    QPushButton* offkey;
    QGroupBox* EmuSettings;
    QComboBox* InCon;
    QComboBox* OutCon;
    QLabel* text_incon;
    QLabel* text_outcon;
    QCheckBox* maxspeed;
    QCheckBox* regdump;
    QButtonGroup* flags;
    QCheckBox* carry;
    QCheckBox* zero;
    QGroupBox* display;
    QLCDNumber* digit1;
    QLCDNumber* digit2;
    QLCDNumber* digit3;
    QLCDNumber* digit4;
    QLCDNumber* digit5;
    QLCDNumber* digit6;
    QLabel* microtronic;
    QButtonGroup* HexKeys;
    QPushButton* key1;
    QPushButton* key2;
    QPushButton* key3;
    QPushButton* key4;
    QPushButton* key5;
    QPushButton* key6;
    QPushButton* key7;
    QPushButton* key8;
    QPushButton* key9;
    QPushButton* keyB;
    QPushButton* keyC;
    QPushButton* keyD;
    QPushButton* keyE;
    QPushButton* keyA;
    QPushButton* keyF;
    QPushButton* key0;
    QButtonGroup* ControlKeys;
    QPushButton* pgm_key;
    QPushButton* run_key;
    QPushButton* halt_key;
    QPushButton* bkp_key;
    QPushButton* step_key;
    QPushButton* next_key;
    QPushButton* reg_key;
    QPushButton* cce_key;

public slots:
    virtual void keyPressEvent( QKeyEvent * e );
    virtual void keyReleaseEvent( QKeyEvent * e );
    virtual void reset_key_pressed();
    virtual void reset_key_released();
    virtual void HexKey_released( int k );
    virtual void CKey_released( int k );
    virtual void GetLoadProgramName();
    virtual void GetSaveProgramName();
    virtual void customEvent( QCustomEvent * e );
    virtual void maxspeed_toggled( bool state );
    virtual void InConChanged();
    virtual void regdump_toggled( bool state );
    virtual void in1_toggled( bool state );
    virtual void in2_toggled( bool state );
    virtual void in3_toggled( bool state );
    virtual void in4_toggled( bool state );
    virtual void out1_toggled( bool state );
    virtual void out2_toggled( bool state );
    virtual void out3_toggled( bool state );
    virtual void out4_toggled( bool state );

protected:
    QVBoxLayout* mainformLayout;
    QHBoxLayout* layout8;
    QSpacerItem* spacer9;
    QSpacerItem* spacer10;
    QHBoxLayout* OutputLayout;
    QHBoxLayout* InputLayout;
    QHBoxLayout* ClockLayout;
    QSpacerItem* spacer2;
    QSpacerItem* spacer3;
    QHBoxLayout* resetLayout;
    QSpacerItem* spacer5;
    QSpacerItem* spacer6;
    QHBoxLayout* offLayout;
    QSpacerItem* spacer7;
    QSpacerItem* spacer8;
    QHBoxLayout* EmuSettingsLayout;
    QSpacerItem* spacer17;
    QSpacerItem* spacer11;
    QGridLayout* layout10;
    QVBoxLayout* layout11;
    QHBoxLayout* layout8_2;
    QVBoxLayout* layout51;
    QHBoxLayout* layout50;
    QVBoxLayout* flagsLayout;
    QSpacerItem* spacer12;
    QGridLayout* displayLayout;
    QHBoxLayout* layout49;
    QGridLayout* HexKeysLayout;
    QGridLayout* ControlKeysLayout;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // MAINFORM_H
