/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

#include <sys/time.h>
#include <iostream>

#include <qbutton.h>
#include <qfiledialog.h>
#include <qapplication.h>

#include "Combos.h"
#include "MyEvents.h"
#include "MICclock.h"
#include "MIC2090.h"
#include "stdin_thread.h"

using namespace std;


extern QApplication *pA;
extern clock2090 CLK;
extern core2090 MIC;
extern STDIN_thread CIN;


#define WAITTIME 100


void mainform::keyPressEvent( QKeyEvent * e )
{
   switch ( InCon->currentItem() )
   {

      case F14_ON_14:
      {
         switch ( e->key() )
         {
            case Qt::Key_F1 : { in1->setChecked(true);
                                e->accept();
                                return;
                              }
            case Qt::Key_F2 : { in2->setChecked(true);
                                e->accept();
                                return;
                              }
            case Qt::Key_F3 : { in3->setChecked(true);
                                e->accept();
                                return;
                              }
            case Qt::Key_F4 : { in4->setChecked(true);
                                e->accept();
                                return;
                              }
         }
         break;
      }

      case F13CLOCK_ON_14:
      {
         switch ( e->key() )
         {
            case Qt::Key_F1 : { in1->setChecked(true);
                                e->accept();
                                return;
                              }
            case Qt::Key_F2 : { in2->setChecked(true);
                                e->accept();
                                return;
                              }
            case Qt::Key_F3 : { in3->setChecked(true);
                                e->accept();
                                return;
                              }
         }
         break;
      }

      case CLKF12STDIN_ON_14:
      {
         switch ( e->key() )
         {
            case Qt::Key_F1 : { in2->setChecked(true);
                                e->accept();
                                return;
                              }
            case Qt::Key_F2 : { in3->setChecked(true);
                                e->accept();
                                return;
                              }
         }
         break;
      }

   }
}


void mainform::keyReleaseEvent( QKeyEvent * e )
{
   switch ( InCon->currentItem() )
   {

      case F14_ON_14:
      {
         switch ( e->key() )
         {
            case Qt::Key_F1 : { in1->setChecked(false);
                                e->accept();
                                return;
                              }
            case Qt::Key_F2 : { in2->setChecked(false);
                                e->accept();
                                return;
                              }
            case Qt::Key_F3 : { in3->setChecked(false);
                                e->accept();
                                return;
                              }
            case Qt::Key_F4 : { in4->setChecked(false);
                                e->accept();
                                return;
                              }
         }
         break;
      }

      case F13CLOCK_ON_14:
      {
         switch ( e->key() )
         {
            case Qt::Key_F1 : { in1->setChecked(false);
                                e->accept();
                                return;
                              }
            case Qt::Key_F2 : { in2->setChecked(false);
                                e->accept();
                                return;
                              }
            case Qt::Key_F3 : { in3->setChecked(false);
                                e->accept();
                                return;
                              }
         }
         break;
      }

      case CLKF12STDIN_ON_14:
      {
         switch ( e->key() )
         {
            case Qt::Key_F1 : { in2->setChecked(false);
                                e->accept();
                                return;
                              }
            case Qt::Key_F2 : { in3->setChecked(false);
                                e->accept();
                                return;
                              }
         }
         break;
      }

   }
}


void mainform::reset_key_pressed()
{
   CLK.go_on = false;

   MIC.go_on = false;
   MIC.keystroke = HALT;

   CLK.wait(WAITTIME);
   MIC.wait(WAITTIME);

   CLK.time_val.lock();
   pA->lock();
   MIC.terminate();
   pA->unlock();
   CLK.time_val.unlock();
}


void mainform::reset_key_released()
{
   CLK.wait(WAITTIME);
   MIC.wait(WAITTIME);
   if ( !CLK.running() ) CLK.start();
   if ( !MIC.running() ) MIC.start();
}


void mainform::HexKey_released( int k )
{
   MIC.keystroke = (Keys2090)k;
}


void mainform::CKey_released( int k )
{
   switch (k)
   {
      case 0: { MIC.keystroke = CCE;  return; }
      case 1: { MIC.keystroke = PGM;  return; }
      case 2: { MIC.keystroke = RUN;  return; }
      case 3: { MIC.keystroke = HALT; return; }
      case 4: { MIC.keystroke = BKP;  return; }
      case 5: { MIC.keystroke = STEP; return; }
      case 6: { MIC.keystroke = NEXT; return; }
      case 7: { MIC.keystroke = REG;  return; }
   }
   cerr << "Internal error: invalid key!" << k << endl;
}


void mainform::GetLoadProgramName()
{
   QFileDialog* fd = new QFileDialog(
                     "./", 
                     "Microtronic programs (*.mic *.MIC);;All files (*)",
                     this,
                     "load file dialog",
                     true );
   fd->setMode( QFileDialog::ExistingFile );
   fd->setCaption("Load Microtronic Program");

   if ( fd->exec() == QDialog::Accepted )
      MIC.filename = fd->selectedFile();
   else
      MIC.filename = "";

   delete fd;

   MIC.lock = false;

   return;
}


void mainform::GetSaveProgramName()
{
   QFileDialog* fd = new QFileDialog(
                     "./", 
                     "Microtronic programs (*.mic *.MIC);;All files (*)",
                     this,
                     "save file dialog",
                     true );
   fd->setMode( QFileDialog::AnyFile );
   fd->setCaption("Save Microtronic Program");

   if ( fd->exec() == QDialog::Accepted )
      MIC.filename = fd->selectedFile();
   else
      MIC.filename = "";

   delete fd;

   MIC.lock = false;

   return;
}


void mainform::customEvent( QCustomEvent *e )
{
   switch (e->type())
   {
      case UPDATE_EVENT:
      {
         update();
         break;
      }
      case SAVE_EVENT:
      {
         GetSaveProgramName();
         break;
      }
      case LOAD_EVENT:
      {
         GetLoadProgramName();
         break;
      }
      default:
      {
         cerr << "Internal error: unknown event " << e->type() << "!" << endl;
         break;
      }
   }
}


void mainform::maxspeed_toggled( bool state )
{
   MIC.max_speed = state;
}


void mainform::InConChanged()
{
   CIN.go_on = false;
   CIN.wait(WAITTIME);

   switch( InCon->currentItem() )
   {
      case IN_NONE:
      {
         in1->setEnabled(true);
         in2->setEnabled(true);
         in3->setEnabled(true);
         in4->setEnabled(true);
         break;
      }
      case F14_ON_14:
      {
         in1->setEnabled(false);
         in2->setEnabled(false);
         in3->setEnabled(false);
         in4->setEnabled(false);
         break;
      }
      case STDIN:
      {
         in1->setEnabled(false);
         in2->setEnabled(false);
         in3->setEnabled(false);
         in4->setEnabled(false);

         if ( !CIN.running() ) CIN.start();

         break;
      }
      case CLOCK_ON_1:
      {
         in1->setEnabled(false);
         in2->setEnabled(true);
         in3->setEnabled(true);
         in4->setEnabled(true);
         break;
      }
      case CLOCK_ON_2:
      {
         in1->setEnabled(true);
         in2->setEnabled(false);
         in3->setEnabled(true);
         in4->setEnabled(true);
         break;
      }
      case CLOCK_ON_3:
      {
         in1->setEnabled(true);
         in2->setEnabled(true);
         in3->setEnabled(false);
         in4->setEnabled(true);
         break;
      }
      case CLOCK_ON_4:
      {
         in1->setEnabled(true);
         in2->setEnabled(true);
         in3->setEnabled(true);
         in4->setEnabled(false);
         break;
      }
      case OUTPUTS_TO_INPUTS:
      {
         in1->setEnabled(false);
         in2->setEnabled(false);
         in3->setEnabled(false);
         in4->setEnabled(false);

         in1->setChecked( out1->isChecked() );
         in2->setChecked( out2->isChecked() );
         in3->setChecked( out3->isChecked() );
         in4->setChecked( out4->isChecked() );

         break;
      }
      case F13CLOCK_ON_14:
      {
         in1->setEnabled(false);
         in2->setEnabled(false);
         in3->setEnabled(false);
         in4->setEnabled(false);
         break;
      }
      case CLKF12STDIN_ON_14:
      {
         in1->setEnabled(false);
         in2->setEnabled(false);
         in3->setEnabled(false);
         in4->setEnabled(false);

         if ( !CIN.running() ) CIN.start();

         break;
      }
   }
}


void mainform::regdump_toggled( bool state )
{
   MIC.register_dump = state;
}


void mainform::in1_toggled( bool state )
{
   MIC.in[0] = state;
}


void mainform::in2_toggled( bool state )
{
   MIC.in[1] = state;
}


void mainform::in3_toggled( bool state )
{
   MIC.in[2] = state;
}


void mainform::in4_toggled( bool state )
{
   MIC.in[3] = state;
   if ( state ) CLK.increment();
}


void mainform::out1_toggled( bool state )
{
   if ( InCon->currentItem() == OUTPUTS_TO_INPUTS )
      in1->setChecked( state );
}


void mainform::out2_toggled( bool state )
{
   if ( InCon->currentItem() == OUTPUTS_TO_INPUTS )
      in2->setChecked( state );
}


void mainform::out3_toggled( bool state )
{
   if ( InCon->currentItem() == OUTPUTS_TO_INPUTS )
      in3->setChecked( state );
}


void mainform::out4_toggled( bool state )
{
   if ( InCon->currentItem() == OUTPUTS_TO_INPUTS )
      in4->setChecked( state );
}
