//
// Combos.h
// Copyright (C) 2.10.2005 Ingo D. Rullhusen <d01c@uni-bremen.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//

#ifndef COMBOS_H
#define COMBOS_H

#include <qcombobox.h>

enum OutCon
{
   OUT_NONE = 0,
   STDOUT,
   BUZZER_ON_1,
   BUZZER_ON_2,
   BUZZER_ON_3,
   BUZZER_ON_4
};

enum InCon
{
   IN_NONE = 0,
   STDIN,
   CLOCK_ON_1,
   CLOCK_ON_2,
   CLOCK_ON_3,
   CLOCK_ON_4,
   OUTPUTS_TO_INPUTS,
   F14_ON_14,
   F13CLOCK_ON_14,
   CLKF12STDIN_ON_14
};

#endif
