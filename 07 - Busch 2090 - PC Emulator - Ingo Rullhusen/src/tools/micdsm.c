/*
*   micdsm.c
*/
#define VERSION "20.8.2005"
/*
*   disassemble a MIC file
*/
#define CPR "(c) 2005 Dr.-Ing. Ingo D. Rullhusen <d01c@uni-bremen.de>"
/*
* This program is free software; you can redistribute it and/or modify   
* it under the terms of the GNU General Public License as published by   
* the Free Software Foundation; either version 2 of the License, or   
* (at your option) any later version.   
* 
* This program is distributed in the hope that it will be useful,   
* but WITHOUT ANY WARRANTY; without even the implied warranty of   
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
* GNU General Public License for more details.   
*
* You should have received a copy of the GNU General Public License   
* along with this program; if not, write to the Free Software   
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*
*/

#include <stdio.h>
#include <stdlib.h>


unsigned char hex2ui( char hex )
{
   switch( hex )
   {
      case '0': { return(0x0); }
      case '1': { return(0x1); }
      case '2': { return(0x2); }
      case '3': { return(0x3); }
      case '4': { return(0x4); }
      case '5': { return(0x5); }
      case '6': { return(0x6); }
      case '7': { return(0x7); }
      case '8': { return(0x8); }
      case '9': { return(0x9); }
      case 'A':
      case 'a': { return(0xa); }
      case 'B':
      case 'b': { return(0xb); }
      case 'C':
      case 'c': { return(0xc); }
      case 'D':
      case 'd': { return(0xd); }
      case 'E':
      case 'e': { return(0xe); }
      case 'F':
      case 'f': { return(0xf); }
   }
   return(0xff);
}

void mnemonic( char* M, char *op1, char *op2, int *addr,
               char c1, char c2, char c3 )
{

   switch( c1 )
   {
      case '0':
      {
         sprintf(M,"MOV  ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '1':
      {
         sprintf(M,"MOVI ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '2':
      {
         sprintf(M,"AND  ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '3':
      {
         sprintf(M,"ANDI ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '4':
      {
         sprintf(M,"ADD  ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '5':
      {
         sprintf(M,"ADDI ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '6':
      {
         sprintf(M,"SUB  ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '7':
      {
         sprintf(M,"SUBI ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '8':
      {
         sprintf(M,"CMP  ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case '9':
      {
         sprintf(M,"CMPI ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case 'A':
      {
         sprintf(M,"OR   ");
         *op1 = c2;
         *op2 = c3;
         *addr = -1;
         break;
      }
      case 'B':
      {
         sprintf(M,"CALL ");
         *op1 = ' ';
         *op2 = ' ';
         *addr = hex2ui(c2)*16 + hex2ui(c3);
         break;
      }
      case 'C':
      {
         sprintf(M,"GOTO ");
         *op1 = ' ';
         *op2 = ' ';
         *addr = hex2ui(c2)*16 + hex2ui(c3);
         break;
      }
      case 'D':
      {
         sprintf(M,"BRC  ");
         *op1 = ' ';
         *op2 = ' ';
         *addr = hex2ui(c2)*16 + hex2ui(c3);
         break;
      }
      case 'E':
      {
         sprintf(M,"BRZ  ");
         *op1 = ' ';
         *op2 = ' ';
         *addr = hex2ui(c2)*16 + hex2ui(c3);
         break;
      }

      case 'F':
      {
         switch(c2)
         {
            case '0':
            {
               switch (c3)
               {
                  case '0':
                  {
                     sprintf(M,"HALT");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '1':
                  {
                     sprintf(M,"NOP");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '2':
                  {
                     sprintf(M,"DISOUT");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '3':
                  {
                     sprintf(M,"HXDZ");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '4':
                  {
                     sprintf(M,"DZHX");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '5':
                  {
                     sprintf(M,"RND");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '6':
                  {
                     sprintf(M,"TIME");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '7':
                  {
                     sprintf(M,"RET");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '8':
                  {
                     sprintf(M,"CLEAR");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case '9':
                  {
                     sprintf(M,"STC");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case 'A':
                  {
                     sprintf(M,"RSC");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case 'B':
                  {
                     sprintf(M,"MULT");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case 'C':
                  {
                     sprintf(M,"DIV");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case 'D':
                  {
                     sprintf(M,"EXRL");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case 'E':
                  {
                     sprintf(M,"EXRM");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }
                  case 'F':
                  {
                     sprintf(M,"EXRA");
                     *op1 = ' ';
                     *op2 = ' ';
                     *addr = -1;
                     break;
                  }

                  default:
                  {
                     sprintf(M,"!!! ERROR !!!");
                  }
               }

               break;
            }

            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            {
               sprintf(M,"DISP ");
               *op1 = c2;
               *op2 = c3;
               *addr = -1;
               break;
            }

            case '7':
            {
               sprintf(M,"MAS  ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }
            case '8':
            {
               sprintf(M,"INV  ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }
            case '9':
            {
               sprintf(M,"SHR  ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }
            case 'A':
            {
               sprintf(M,"SHL  ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }
            case 'B':
            {
               sprintf(M,"ADC  ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }
            case 'C':
            {
               sprintf(M,"SUBC ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }
            case 'D':
            {
               sprintf(M,"DIN  ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }
            case 'E':
            {
               sprintf(M,"DOT  ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }
            case 'F':
            {
               sprintf(M,"KIN  ");
               *op1 = c3;
               *op2 = ' ';
               *addr = -1;
               break;
            }

            default:
            {
               sprintf(M,"!!! ERROR !!!");
            }
         }

         break;
      }

      default:
      {
         sprintf(M,"!!! ERROR !!!");
      }
   }
}


int main( int argc, char *argv[] )
{
   int lno, fno;
   char buff[256][4];
   char M[256][16], op1[256], op2[256];
   int addr[256];
   FILE *micfile;

   if ( argc != 2 )
   {
      printf("micdsm (version %s)\n",VERSION);
      printf(" Copyright %s\n",CPR);
      printf(" micdsm comes with ABSOLUTELY NO WARRANTY; for details see GNU\n");
      printf(" GENERAL PUBLIC LICENSE. This is free software, and you are welcome to\n");
      printf(" redistribute it under certain conditions; see GNU GENERAL PUBLIC LICENSE\n");
      printf(" for details.\n\n");

      printf("micdsm <MIC file>\n");
      return(0);
   }

   micfile = fopen( argv[1], "r" );
   if ( ! micfile )
   {
      fprintf(stderr,"Cannot open file %s!\n",argv[1]);
      return(2);
   }

   for ( lno=0; lno<256; lno++ )
   {
      buff[lno][0] = (char)getc( micfile );
      buff[lno][1] = (char)getc( micfile );
      buff[lno][2] = (char)getc( micfile );
      buff[lno][3] = (char)getc( micfile );

      if ( feof(micfile) )
      {
         fprintf(stderr,"MIC file incomplete!\n");
         return(5);
      }

      if ( buff[lno][3] != '\r' )
      {
         fprintf(stderr,"MIC file inconsistent!\n");
         return(5);
      }

      mnemonic( M[lno], &(op1[lno]), &(op2[lno]), &(addr[lno]),
                buff[lno][0],buff[lno][1],buff[lno][2] );
   }


/* labels */
   buff[0][3] = '_';
   for ( lno=0; lno<256; lno++ )
   {
      if ( addr[lno] > -1 ) buff[ addr[lno] ][3] = '_';
   }

/* fill */
   for ( fno=256; fno>1; fno-- )
   {
      if ( buff[255][0] != buff[fno-2][0] ||
           buff[255][1] != buff[fno-2][1] ||
           buff[255][2] != buff[fno-2][2] ||
           buff[fno-1][3] == '_'             ) break;
   }


/* output */
   printf(";\n");
   printf("; disassembly of \"%s\"\n",argv[1]);
   printf(";\n\n");

   if ( fno < 256 )
   {
      printf("fill = %c%c%c\n", buff[255][0],buff[255][1],buff[255][2] );
   }

   printf("00   = _00\n");

   for ( lno=0; lno<fno; lno++ )
   {
      if ( buff[lno][3] == '_' )
      {
         printf("\n");
         printf("_%.2X: ",lno);
      }
      else
      {
         printf("     ");
      }

      printf("%s",M[lno]);

      if ( op1[lno] != ' ' )
      {
         printf("%c",op1[lno]);
         if ( op2[lno] != ' ' )
         {
            printf(",%c",op2[lno]);
         }
      }
      else if ( addr[lno] > -1 )
      {
          printf("_%.2X",addr[lno]);
      }

      printf("\n");

   }


   fclose(micfile);
   return(0);
}
