/*
*   wav2mic.c
*/
#define VERSION "27.8.2005"
/*
*   convert WAV file from Kassentten-Interface 2095 into a MIC file
*/
#define CPR "(c) 2005 Dr.-Ing. Ingo D. Rullhusen <d01c@uni-bremen.de>"
/*
* This program is free software; you can redistribute it and/or modify   
* it under the terms of the GNU General Public License as published by   
* the Free Software Foundation; either version 2 of the License, or   
* (at your option) any later version.   
* 
* This program is distributed in the hope that it will be useful,   
* but WITHOUT ANY WARRANTY; without even the implied warranty of   
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
* GNU General Public License for more details.   
*
* You should have received a copy of the GNU General Public License   
* along with this program; if not, write to the Free Software   
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*
*/


#include <stdlib.h>
#include <stdio.h>


#define REL_THR 0.7
#define REL_TOL 0.25
#define NO_PLSE 9


#define FREQ_0  568.0
#define FREQ_1 1136.0
#define FREQ_2 2272.0

#define T_INIT 2.0

#define T_BIT   0.063
#define T_BIT_F 0.047

#define T_SYNC_0 0.030
#define T_SYNC_1 0.170


struct WAV_HEADER
{
   char riff[4];
   unsigned int  f_length_m8;
   char wave[4];
   char fmt_[4];
   unsigned int  fmt_length;
   unsigned short format;
   unsigned short channels;
   unsigned int sample_rate;
   unsigned int byte_per_sec;
   unsigned short block_align;
   unsigned short bits_per_sample;
   char data[4];
   unsigned int data_length;
} wav_header;


char ui2hex( unsigned char ui )
{
   switch( ui )
   {
      case 0x0: { return('0'); }
      case 0x1: { return('1'); }
      case 0x2: { return('2'); }
      case 0x3: { return('3'); }
      case 0x4: { return('4'); }
      case 0x5: { return('5'); }
      case 0x6: { return('6'); }
      case 0x7: { return('7'); }
      case 0x8: { return('8'); }
      case 0x9: { return('9'); }
      case 0xa: { return('A'); }
      case 0xb: { return('B'); }
      case 0xc: { return('C'); }
      case 0xd: { return('D'); }
      case 0xe: { return('E'); }
      case 0xf: { return('F'); }
   }
   return(' ');
}


double get_next_edge( FILE *wavfile )
{
   unsigned long int pos;
   unsigned char bmin, b;

   pos = 0;
   bmin = 255;

   while ( ! feof(wavfile) )
   {
      fread((void*)&b,sizeof(b),1,wavfile);
      pos++;

      if ( b < bmin )
         bmin = b;
      else if ( b - bmin > ( 255 - 2*bmin ) * REL_THR && bmin < 128 )
         return( ((double)pos)/((double)wav_header.sample_rate) );
   }

   return(-1.0);
}


int scan4initseq( FILE *wavfile )
{
   unsigned long int spno, spt;
   double tpos, iiv;

   iiv = 1.0/FREQ_2;

   spt = (unsigned long int)( T_INIT / iiv );
   spno = 0;

   while ( ! feof(wavfile) )
   {
      tpos = get_next_edge(wavfile);
      if ( tpos < 0.0 ) return(1);

      if ( tpos < (1.0+REL_TOL) * iiv &&
           tpos > (1.0-REL_TOL) * iiv    )
      {
         spno++;
      }
      else
      {
         spno = 0;
      }

      if ( spno > spt ) return(0);
   }

   return(1);
}

int scan4syncseq( FILE *wavfile )
{
   unsigned long int spno;
   double tpos, iiv;

   iiv = 1.0/FREQ_2;

   spno = 0;

   while ( ! feof(wavfile) )
   {
      tpos = get_next_edge(wavfile);
      if ( tpos < 0.0 ) return(1);

      if ( tpos < (1.0+REL_TOL) * iiv &&
           tpos > (1.0-REL_TOL) * iiv    )
      {
         spno++;
      }
      else
      {
         spno = 0;
      }

      if ( spno > NO_PLSE ) return(0);
   }

   return(1);
}


int skipseq( FILE *wavfile, double len )
{
   long no = (long)( len * wav_header.sample_rate );

   return( fseek( wavfile, no, SEEK_CUR ) );
}


int get_bit( FILE *wavfile )
{
   unsigned long int spno0, spno1;
   double tpos, iiv0, iiv1;

   iiv0 = 1.0/FREQ_0;
   iiv1 = 1.0/FREQ_1;

   spno0 = 0;
   spno1 = 0;

   while ( ! feof(wavfile) )
   {
      tpos = get_next_edge(wavfile);
      if ( tpos < 0 || tpos > 2.0*(1.0+REL_TOL)/FREQ_0 ) return(-1);

      if ( tpos < (1.0+REL_TOL) * iiv0 &&
           tpos > (1.0-REL_TOL) * iiv0    )
      {
         spno0++;
      }
      else
      {
         spno0 = 0;
      }

      if ( tpos < (1.0+REL_TOL) * iiv1 &&
           tpos > (1.0-REL_TOL) * iiv1    )
      {
         spno1++;
      }
      else
      {
         spno1 = 0;
      }

      if ( spno0 > NO_PLSE || spno1 > NO_PLSE ) break;
   }

   if (      spno0 > NO_PLSE && spno1 == 0 ) return(0);
   else if ( spno1 > NO_PLSE && spno0 == 0 ) return(1);

   return(-1);
}


int readprogline( FILE *wavfile, char *c )
{
   unsigned char bc, mask;
   int i, bit;
   double clen;

/* first bit */
   if ( (bit = get_bit( wavfile )) < 0 ) return(1);

   bc = bit * 0x1;

   clen = (double)( NO_PLSE + 1 ) / ( bit ? FREQ_1 : FREQ_0 );

/* next 3 bits */
   mask = 0x2;
   for ( i=0; i<3; i++ )
   {
      if ( skipseq( wavfile, T_BIT-clen ) ) return(1);

      if ( (bit = get_bit( wavfile )) < 0 ) return(1);

      if ( bit ) bc |= mask;
      mask <<= 1;

      clen = (double)( NO_PLSE + 1 ) / ( bit ? FREQ_1 : FREQ_0 );
   }

   c[0] = ui2hex( bc );

/* next 4 bits */
   mask = 0x1;
   bc = 0;
   for ( i=0; i<4; i++ )
   {
      if ( skipseq( wavfile, T_BIT-clen ) ) return(1);

      if ( (bit = get_bit( wavfile )) < 0 ) return(1);

      if ( bit ) bc |= mask;
      mask <<= 1;

      clen = (double)( NO_PLSE + 1 ) / ( bit ? FREQ_1 : FREQ_0 );
   }

   c[1] = ui2hex( bc );

/* next 4 bits */
   mask = 0x1;
   bc = 0;
   for ( i=0; i<4; i++ )
   {
      if ( skipseq( wavfile, T_BIT-clen ) ) return(1);

      if ( (bit = get_bit( wavfile )) < 0 ) return(1);

      if ( bit ) bc |= mask;
      mask <<= 1;

      clen = (double)( NO_PLSE + 1 ) / ( bit ? FREQ_1 : FREQ_0 );
   }

   c[2] = ui2hex( bc );

   return(0);
}


int main( int argc, char *argv[] )
{
   int progr_no, lno;
   char buff[4], name[24];
   FILE *micfile, *wavfile;

   if ( argc != 2 )
   {
      printf("wav2mic (version %s)\n",VERSION);
      printf(" Copyright %s\n",CPR);
      printf(" wav2mic comes with ABSOLUTELY NO WARRANTY; for details see GNU\n");
      printf(" GENERAL PUBLIC LICENSE. This is free software, and you are welcome to\n");
      printf(" redistribute it under certain conditions; see GNU GENERAL PUBLIC LICENSE\n");
      printf(" for details.\n\n");

      printf("wav2mic <WAV file>\n");
      return(0);
   }

   wavfile = fopen( argv[1], "r" );
   if ( ! wavfile )
   {
      fprintf(stderr,"Cannot open file %s!\n",argv[1]);
      return(2);
   }

   fread((void*)&wav_header,sizeof(wav_header),1,wavfile);

   if ( wav_header.riff[0] != 'R'
        || wav_header.riff[1] != 'I'
           || wav_header.riff[2] != 'F'
              || wav_header.riff[3] != 'F' ||
        wav_header.wave[0] != 'W'
        || wav_header.wave[1] != 'A'
           || wav_header.wave[2] != 'V'
              || wav_header.wave[3] != 'E' ||
        wav_header.fmt_[0] != 'f'
        || wav_header.fmt_[1] != 'm'
           || wav_header.fmt_[2] != 't'
              || wav_header.fmt_[3] != ' ' ||
        wav_header.fmt_length != 16 ||
        wav_header.format != 1 ||
        wav_header.channels != 1 ||
        wav_header.bits_per_sample != 8 ||
        wav_header.data[0] != 'd' 
        || wav_header.data[1] != 'a'
           || wav_header.data[2] != 't'
              || wav_header.data[3] != 'a'
      )
   {
      printf("Error: input not a simple RIFF WAVE PCM 8 bit mono!\n");
      return(5);
   }

   printf("Input sample rate: %u Hz\n", wav_header.sample_rate );

   progr_no = 0;
   buff[3] = '\r';
   while ( !feof(wavfile) )
   {
      if ( ! scan4initseq(wavfile) )
      {
         progr_no++;
         sprintf(name,"program%.3i.mic",progr_no);
         printf("Found file: opening %s ...\n",name);

         micfile = fopen( name, "w" );
         if ( ! micfile )
         {
            fprintf(stderr,"Cannot open file %s!\n",name);
            return(2);
         }
      }
      else break; /* no programs found */

      for ( lno=0; lno<256; lno++ )
      {
         if ( scan4syncseq( wavfile ) ) break;

         if ( ! readprogline( wavfile, buff ) )
         {
            fwrite((void*)buff,sizeof(char),4,micfile);
         }
         else break;
      }

      if ( lno == 0 )
      {
         progr_no--;
         printf("File empty: %s cancelled.\n",name);
      }
      else
      {
         buff[0] = buff[1] = buff[2] = '0';
         for ( ; lno<256; lno++ )
         {
            fwrite((void*)buff,sizeof(char),4,micfile);
         }
      }

      fclose(micfile);
   }

   fclose(wavfile);
   return(0);
}
