/* A Bison parser, made from micasmY.y
   by GNU bison 1.35.  */

#define YYBISON 1  /* Identify Bison output.  */

# define	CHAR_0	257
# define	CHAR_1_6	258
# define	CHAR_7_F	259
# define	QUOTED	260
# define	LABEL	261
# define	COMMEND	262
# define	COMMA	263
# define	FILL	264
# define	OUTPUT	265
# define	COLON	266
# define	MOV	267
# define	MOVI	268
# define	AND	269
# define	ANDI	270
# define	ADD	271
# define	ADDI	272
# define	SUB	273
# define	SUBI	274
# define	CMP	275
# define	CMPI	276
# define	OR	277
# define	CALL	278
# define	GOTO	279
# define	BRC	280
# define	BRZ	281
# define	HALT	282
# define	NOP	283
# define	DISOUT	284
# define	HXDZ	285
# define	DZHX	286
# define	RND	287
# define	TIME	288
# define	RET	289
# define	CLEAR	290
# define	STC	291
# define	RSC	292
# define	MULT	293
# define	DIV	294
# define	EXRL	295
# define	EXRM	296
# define	EXRA	297
# define	DISP	298
# define	MAS	299
# define	INV	300
# define	SHR	301
# define	SHL	302
# define	ADC	303
# define	SUBC	304
# define	DIN	305
# define	DOT	306
# define	KIN	307
# define	NEWL	308
# define	EQ	309
# define	NBR	310

#line 24 "micasmY.y"


#include <stdlib.h>
#include <ctype.h>
#include <string.h>


char memory[256][3];

char *filename;
int lno;
char fill[3];
char *label[256], *pre_label[256];

int error_line = 0;
char *error_msg = NULL;


int yyerror(char *s);

extern int yylex();



#line 53 "micasmY.y"
#ifndef YYSTYPE
typedef union { char chr;
         char*  chstr;
         char chr3[3];
       } yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
#ifndef YYDEBUG
# define YYDEBUG 0
#endif



#define	YYFINAL		129
#define	YYFLAG		-32768
#define	YYNTBASE	57

/* YYTRANSLATE(YYLEX) -- Bison token number corresponding to YYLEX. */
#define YYTRANSLATE(x) ((unsigned)(x) <= 310 ? yytranslate[x] : 68)

/* YYTRANSLATE[YYLEX] -- Bison token number corresponding to YYLEX. */
static const char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56
};

#if YYDEBUG
static const short yyprhs[] =
{
       0,     0,     2,     5,     6,     8,    10,    12,    14,    19,
      24,    29,    32,    35,    38,    41,    46,    51,    56,    61,
      66,    71,    76,    81,    86,    91,    96,    98,   100,   102,
     104,   106,   108,   110,   112,   114,   116,   118,   120,   122,
     124,   126,   128,   133,   136,   139,   142,   145,   148,   151,
     154,   157,   160,   163,   166,   169,   172,   174,   176,   180,
     183,   185,   187,   189
};
static const short yyrhs[] =
{
      58,     0,    58,    59,     0,     0,    54,     0,    60,     0,
      61,     0,     8,     0,    10,    55,    64,    54,     0,    11,
      55,     6,    54,     0,    65,    55,     7,    54,     0,     7,
      12,     0,    65,    12,     0,    62,    54,     0,    56,    64,
       0,    13,    66,     9,    66,     0,    14,    66,     9,    66,
       0,    15,    66,     9,    66,     0,    16,    66,     9,    66,
       0,    17,    66,     9,    66,     0,    18,    66,     9,    66,
       0,    19,    66,     9,    66,     0,    20,    66,     9,    66,
       0,    21,    66,     9,    66,     0,    22,    66,     9,    66,
       0,    23,    66,     9,    66,     0,    28,     0,    29,     0,
      30,     0,    31,     0,    32,     0,    33,     0,    34,     0,
      35,     0,    36,     0,    37,     0,    38,     0,    39,     0,
      40,     0,    41,     0,    42,     0,    43,     0,    44,    67,
       9,    66,     0,    45,    66,     0,    46,    66,     0,    47,
      66,     0,    48,    66,     0,    49,    66,     0,    50,    66,
       0,    51,    66,     0,    52,    66,     0,    53,    66,     0,
      24,    63,     0,    25,    63,     0,    26,    63,     0,    27,
      63,     0,    65,     0,     7,     0,    66,    66,    66,     0,
      66,    66,     0,     3,     0,    67,     0,     5,     0,     4,
       0
};

#endif

#if YYDEBUG
/* YYRLINE[YYN] -- source line where rule number YYN was defined. */
static const short yyrline[] =
{
       0,    78,    81,    82,    85,    86,    87,    88,    92,    98,
     105,   118,   151,   166,   181,   186,   191,   196,   201,   206,
     211,   216,   221,   226,   231,   236,   241,   246,   251,   256,
     261,   266,   271,   276,   281,   286,   291,   296,   301,   306,
     311,   316,   321,   326,   331,   336,   341,   346,   351,   356,
     361,   366,   372,   377,   382,   387,   394,   398,   431,   438,
     444,   445,   446,   449
};
#endif


#if (YYDEBUG) || defined YYERROR_VERBOSE

/* YYTNAME[TOKEN_NUM] -- String name of the token TOKEN_NUM. */
static const char *const yytname[] =
{
  "$", "error", "$undefined.", "CHAR_0", "CHAR_1_6", "CHAR_7_F", "QUOTED", 
  "LABEL", "COMMEND", "COMMA", "FILL", "OUTPUT", "COLON", "MOV", "MOVI", 
  "AND", "ANDI", "ADD", "ADDI", "SUB", "SUBI", "CMP", "CMPI", "OR", 
  "CALL", "GOTO", "BRC", "BRZ", "HALT", "NOP", "DISOUT", "HXDZ", "DZHX", 
  "RND", "TIME", "RET", "CLEAR", "STC", "RSC", "MULT", "DIV", "EXRL", 
  "EXRM", "EXRA", "DISP", "MAS", "INV", "SHR", "SHL", "ADC", "SUBC", 
  "DIN", "DOT", "KIN", "NEWL", "EQ", "NBR", "file", "lines", "line", 
  "def", "code", "opx", "addr", "hexno3", "hexno2", "hexno", "char_1_6", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives. */
static const short yyr1[] =
{
       0,    57,    58,    58,    59,    59,    59,    59,    60,    60,
      60,    61,    61,    61,    62,    62,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    62,    63,    63,    64,    65,
      66,    66,    66,    67
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN. */
static const short yyr2[] =
{
       0,     1,     2,     0,     1,     1,     1,     1,     4,     4,
       4,     2,     2,     2,     2,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     4,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     1,     3,     2,
       1,     1,     1,     1
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error. */
static const short yydefact[] =
{
       3,     1,    60,    63,    62,     0,     7,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       4,     0,     2,     5,     6,     0,     0,     0,    61,    11,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    57,    52,    56,    53,    54,    55,     0,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    14,
       0,    13,    12,     0,    59,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     9,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    42,    58,    10,     0,     0,     0
};

static const short yydefgoto[] =
{
     127,     1,    52,    53,    54,    55,    74,    89,    75,    57,
      58
};

static const short yypact[] =
{
  -32768,    98,-32768,-32768,-32768,     3,-32768,   -35,   -31,    14,
      14,    14,    14,    14,    14,    14,    14,    14,    14,    14,
       9,     9,     9,     9,-32768,-32768,-32768,-32768,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
      21,    14,    14,    14,    14,    14,    14,    14,    14,    14,
  -32768,    14,-32768,-32768,-32768,   -28,    -1,    14,-32768,-32768,
      14,    22,    18,    20,    32,    34,    35,    36,    37,    38,
      40,    41,    43,-32768,-32768,-32768,-32768,-32768,-32768,    44,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
      14,-32768,-32768,    23,-32768,   -23,     1,    14,    14,    14,
      14,    14,    14,    14,    14,    14,    14,    14,    14,    14,
       2,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,    57,    58,-32768
};

static const short yypgoto[] =
{
  -32768,-32768,-32768,-32768,-32768,-32768,     0,     4,    59,    -9,
      19
};


#define	YYLAST		154


static const short yytable[] =
{
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    92,     2,     3,     4,    59,    73,     2,     3,     4,
      60,    76,    77,    78,    61,     3,    91,    97,    96,    98,
     110,   111,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    99,    90,   100,   101,   102,   103,   104,    94,   105,
     106,    90,   107,   108,    93,   112,   126,   128,   129,    79,
      56,     0,     0,     0,    95,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   109,     0,     0,     0,     0,     0,     0,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,     2,     3,     4,     0,     5,     6,     0,     7,     8,
       0,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,     0,    51
};

static const short yycheck[] =
{
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    12,     3,     4,     5,    12,     7,     3,     4,     5,
      55,    21,    22,    23,    55,     4,    54,     9,     6,     9,
       7,    54,    41,    42,    43,    44,    45,    46,    47,    48,
      49,     9,    51,     9,     9,     9,     9,     9,    57,     9,
       9,    60,     9,     9,    55,    54,    54,     0,     0,    40,
       1,    -1,    -1,    -1,    60,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    90,    -1,    -1,    -1,    -1,    -1,    -1,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,     3,     4,     5,    -1,     7,     8,    -1,    10,    11,
      -1,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    -1,    56
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/share/bison/bison.simple"

/* Skeleton output parser for bison,

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software
   Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser when
   the %semantic_parser declaration is not specified in the grammar.
   It was written by Richard Stallman by simplifying the hairy parser
   used when %semantic_parser is specified.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

#ifndef YYPARSE_RETURN_TYPE
#define YYPARSE_RETURN_TYPE int
#endif

#if ! defined (yyoverflow) || defined (YYERROR_VERBOSE)

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || defined (YYERROR_VERBOSE) */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || ((YYLTYPE_IS_TRIVIAL || ! YYLSP_NEEDED) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
# if YYLSP_NEEDED
  YYLTYPE yyls;
# endif
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAX (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# if YYLSP_NEEDED
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAX)
# else
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAX)
# endif

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAX;	\
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif


#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");			\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).

   When YYLLOC_DEFAULT is run, CURRENT is set the location of the
   first token.  By default, to implement support for ranges, extend
   its range to the last symbol.  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)       	\
   Current.last_line   = Rhs[N].last_line;	\
   Current.last_column = Rhs[N].last_column;
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#if YYPURE
# if YYLSP_NEEDED
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, &yylloc, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval, &yylloc)
#  endif
# else /* !YYLSP_NEEDED */
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval)
#  endif
# endif /* !YYLSP_NEEDED */
#else /* !YYPURE */
# define YYLEX			yylex ()
#endif /* !YYPURE */


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)
/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

#ifdef YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif
#endif

#line 319 "/usr/share/bison/bison.simple"


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
#  define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL
# else
#  define YYPARSE_PARAM_ARG YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
# endif
#else /* !YYPARSE_PARAM */
# define YYPARSE_PARAM_ARG
# define YYPARSE_PARAM_DECL
#endif /* !YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
# ifdef YYPARSE_PARAM
YYPARSE_RETURN_TYPE yyparse (void *);
# else
YYPARSE_RETURN_TYPE yyparse (void);
# endif
#endif

/* YY_DECL_VARIABLES -- depending whether we use a pure parser,
   variables are global, or local to YYPARSE.  */

#define YY_DECL_NON_LSP_VARIABLES			\
/* The lookahead symbol.  */				\
int yychar;						\
							\
/* The semantic value of the lookahead symbol. */	\
YYSTYPE yylval;						\
							\
/* Number of parse errors so far.  */			\
int yynerrs;

#if YYLSP_NEEDED
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES			\
						\
/* Location data for the lookahead symbol.  */	\
YYLTYPE yylloc;
#else
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES
#endif


/* If nonreentrant, generate the variables here. */

#if !YYPURE
YY_DECL_VARIABLES
#endif  /* !YYPURE */

YYPARSE_RETURN_TYPE
yyparse (YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  /* If reentrant, generate the variables here. */
#if YYPURE
  YY_DECL_VARIABLES
#endif  /* !YYPURE */

  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yychar1 = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack. */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;

#if YYLSP_NEEDED
  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
#endif

#if YYLSP_NEEDED
# define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
# define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  YYSIZE_T yystacksize = YYINITDEPTH;


  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
#if YYLSP_NEEDED
  YYLTYPE yyloc;
#endif

  /* When reducing, the number of symbols on the RHS of the reduced
     rule. */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
#if YYLSP_NEEDED
  yylsp = yyls;
#endif
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  */
# if YYLSP_NEEDED
	YYLTYPE *yyls1 = yyls;
	/* This used to be a conditional around just the two extra args,
	   but that might be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
# else
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);
# endif
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
# if YYLSP_NEEDED
	YYSTACK_RELOCATE (yyls);
# endif
# undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
#if YYLSP_NEEDED
      yylsp = yyls + yysize - 1;
#endif

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yychar1 = YYTRANSLATE (yychar);

#if YYDEBUG
     /* We have to keep this `#if YYDEBUG', since we use variables
	which are defined only if `YYDEBUG' is set.  */
      if (yydebug)
	{
	  YYFPRINTF (stderr, "Next token is %d (%s",
		     yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise
	     meaning of a token, for further debugging info.  */
# ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
# endif
	  YYFPRINTF (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %d (%s), ",
	      yychar, yytname[yychar1]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to the semantic value of
     the lookahead token.  This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

#if YYLSP_NEEDED
  /* Similarly for the default location.  Let the user run additional
     commands if for instance locations are ranges.  */
  yyloc = yylsp[1-yylen];
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
#endif

#if YYDEBUG
  /* We have to keep this `#if YYDEBUG', since we use variables which
     are defined only if `YYDEBUG' is set.  */
  if (yydebug)
    {
      int yyi;

      YYFPRINTF (stderr, "Reducing via rule %d (line %d), ",
		 yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (yyi = yyprhs[yyn]; yyrhs[yyi] > 0; yyi++)
	YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
      YYFPRINTF (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif

  switch (yyn) {

case 8:
#line 93 "micasmY.y"
{
         fill[0] = (char)toupper(yyvsp[-1].chr3[0]);
         fill[1] = (char)toupper(yyvsp[-1].chr3[1]);
         fill[2] = (char)toupper(yyvsp[-1].chr3[2]);
      ;
    break;}
case 9:
#line 99 "micasmY.y"
{
         free(filename);
         filename = calloc( sizeof(char), strlen(yyvsp[-1].chstr)+1 );
         strcpy( filename, yyvsp[-1].chstr+1 );
         filename[strlen(filename)-1]  = '\0';
      ;
    break;}
case 10:
#line 106 "micasmY.y"
{
         int no;
         no = hex2ui( yyvsp[-3].chr3[1] )*16 + hex2ui( yyvsp[-3].chr3[2] );
         if ( label[no] != NULL )
         {
            fprintf(stderr,"Multiple assigned label!\n");
         }
         label[no] = calloc( sizeof(char), strlen(yyvsp[-1].chstr)+1 );
         strcpy( label[no], yyvsp[-1].chstr );
      ;
    break;}
case 11:
#line 119 "micasmY.y"
{
          int i;
          for ( i=0; i<256; i++ )
          {
             if ( label[i] )
             {
                if ( strstr(label[i], yyvsp[-1].chstr) && strstr( yyvsp[-1].chstr, label[i] ) ) break;
             }
          }
          if ( i < 256 ) /* label found */
          {
             if ( i < lno )
             {
                fprintf(stderr,"Label address %.2X too small!\n",i);
             }
             for ( ; lno<i; lno++ )
             {
                memory[lno][0] = fill[0];
                memory[lno][1] = fill[1];
                memory[lno][2] = fill[2];
             }
          }
          else /* new label */
          {
             if ( label[lno] != NULL )
             {
               fprintf(stderr,"Internal error!\n");
             }
             label[lno] = calloc( sizeof(char), strlen(yyvsp[-1].chstr)+1 );
             strcpy( label[lno], yyvsp[-1].chstr );
          }
       ;
    break;}
case 12:
#line 152 "micasmY.y"
{
          int i = hex2ui( yyvsp[-1].chr3[1] )*16 + hex2ui( yyvsp[-1].chr3[2] );

          if ( i < lno )
          {
             fprintf(stderr,"Label address %.2X too small!\n",i);
          }
          for ( ; lno<i; lno++ )
          {
             memory[lno][0] = fill[0];
             memory[lno][1] = fill[1];
             memory[lno][2] = fill[2];
          }
       ;
    break;}
case 13:
#line 167 "micasmY.y"
{
          if ( lno > 255 )
          {
             fprintf(stderr,"Program too large!\n");
             lno = 255;
          }
          memory[lno][0] = yyvsp[-1].chr3[0];
          memory[lno][1] = yyvsp[-1].chr3[1];
          memory[lno][2] = yyvsp[-1].chr3[2];
          lno++;
       ;
    break;}
case 14:
#line 182 "micasmY.y"
{   yyval.chr3[0] = yyvsp[0].chr3[0];
          yyval.chr3[1] = yyvsp[0].chr3[1];
          yyval.chr3[2] = yyvsp[0].chr3[2];
      ;
    break;}
case 15:
#line 187 "micasmY.y"
{   yyval.chr3[0] = '0';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 16:
#line 192 "micasmY.y"
{   yyval.chr3[0] = '1';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 17:
#line 197 "micasmY.y"
{   yyval.chr3[0] = '2';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 18:
#line 202 "micasmY.y"
{   yyval.chr3[0] = '3';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 19:
#line 207 "micasmY.y"
{   yyval.chr3[0] = '4';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 20:
#line 212 "micasmY.y"
{   yyval.chr3[0] = '5';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 21:
#line 217 "micasmY.y"
{   yyval.chr3[0] = '6';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 22:
#line 222 "micasmY.y"
{   yyval.chr3[0] = '7';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 23:
#line 227 "micasmY.y"
{   yyval.chr3[0] = '8';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 24:
#line 232 "micasmY.y"
{   yyval.chr3[0] = '9';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 25:
#line 237 "micasmY.y"
{   yyval.chr3[0] = 'A';
          yyval.chr3[1] = yyvsp[-2].chr; 
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 26:
#line 242 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0'; 
          yyval.chr3[2] = '0';
      ;
    break;}
case 27:
#line 247 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '1';
      ;
    break;}
case 28:
#line 252 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '2';
      ;
    break;}
case 29:
#line 257 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '3';
      ;
    break;}
case 30:
#line 262 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '4';
      ;
    break;}
case 31:
#line 267 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '5';
      ;
    break;}
case 32:
#line 272 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '6';
      ;
    break;}
case 33:
#line 277 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '7';
      ;
    break;}
case 34:
#line 282 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '8';
      ;
    break;}
case 35:
#line 287 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = '9';
      ;
    break;}
case 36:
#line 292 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = 'A';
      ;
    break;}
case 37:
#line 297 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = 'B';
      ;
    break;}
case 38:
#line 302 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = 'C';
      ;
    break;}
case 39:
#line 307 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = 'D';
      ;
    break;}
case 40:
#line 312 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = 'E';
      ;
    break;}
case 41:
#line 317 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '0';
          yyval.chr3[2] = 'F';
      ;
    break;}
case 42:
#line 322 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = yyvsp[-2].chr;
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 43:
#line 327 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '7'; 
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 44:
#line 332 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '8';
          yyval.chr3[2] = yyvsp[0].chr;
      ;
    break;}
case 45:
#line 337 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = '9';
          yyval.chr3[2] = yyvsp[0].chr;  
      ;
    break;}
case 46:
#line 342 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = 'A';
          yyval.chr3[2] = yyvsp[0].chr; 
      ;
    break;}
case 47:
#line 347 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = 'B';
          yyval.chr3[2] = yyvsp[0].chr; 
      ;
    break;}
case 48:
#line 352 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = 'C';
          yyval.chr3[2] = yyvsp[0].chr; 
      ;
    break;}
case 49:
#line 357 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = 'D';
          yyval.chr3[2] = yyvsp[0].chr; 
      ;
    break;}
case 50:
#line 362 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = 'E';
          yyval.chr3[2] = yyvsp[0].chr; 
      ;
    break;}
case 51:
#line 367 "micasmY.y"
{   yyval.chr3[0] = 'F';
          yyval.chr3[1] = 'F';
          yyval.chr3[2] = yyvsp[0].chr; 
      ;
    break;}
case 52:
#line 373 "micasmY.y"
{   yyval.chr3[0] = 'B';
          yyval.chr3[1] = yyvsp[0].chr3[1];
          yyval.chr3[2] = yyvsp[0].chr3[2];
      ;
    break;}
case 53:
#line 378 "micasmY.y"
{   yyval.chr3[0] = 'C';
          yyval.chr3[1] = yyvsp[0].chr3[1];
          yyval.chr3[2] = yyvsp[0].chr3[2];
      ;
    break;}
case 54:
#line 383 "micasmY.y"
{   yyval.chr3[0] = 'D';
          yyval.chr3[1] = yyvsp[0].chr3[1];
          yyval.chr3[2] = yyvsp[0].chr3[2];
      ;
    break;}
case 55:
#line 388 "micasmY.y"
{   yyval.chr3[0] = 'E';
          yyval.chr3[1] = yyvsp[0].chr3[1];
          yyval.chr3[2] = yyvsp[0].chr3[2];
      ;
    break;}
case 56:
#line 395 "micasmY.y"
{   yyval.chr3[1] = yyvsp[0].chr3[1];
           yyval.chr3[2] = yyvsp[0].chr3[2];
       ;
    break;}
case 57:
#line 399 "micasmY.y"
{
          int i;
          for ( i=0; i<256; i++ )
          {
             if ( label[i] )
             {
                if ( strstr(label[i], yyvsp[0].chstr) && strstr( yyvsp[0].chstr, label[i] ) ) break;
             }
          }

          if ( i < 256 ) /* label found */
          {
             yyval.chr3[1] = ui2hex( i/16 );
             yyval.chr3[2] = ui2hex( i%16 );
          }
          else /* new label */
          {
             if ( pre_label[lno] ) /* overfull */
             {
               fprintf(stderr,"Internal error!\n");
             }
             else
             {
                pre_label[lno] = calloc( sizeof(char), strlen(yyvsp[0].chstr)+1 );
                strcpy( pre_label[lno], yyvsp[0].chstr );
             }
             yyval.chr3[1] = 'X';
             yyval.chr3[2] = 'Y';
          }
       ;
    break;}
case 58:
#line 432 "micasmY.y"
{   yyval.chr3[0] = yyvsp[-2].chr;
            yyval.chr3[1] = yyvsp[-1].chr;
            yyval.chr3[2] = yyvsp[0].chr;
        ;
    break;}
case 59:
#line 439 "micasmY.y"
{   yyval.chr3[1] = yyvsp[-1].chr;
            yyval.chr3[2] = yyvsp[0].chr;
        ;
    break;}
case 60:
#line 444 "micasmY.y"
{ yyval.chr = yyvsp[0].chr; ;
    break;}
case 61:
#line 445 "micasmY.y"
{ yyval.chr = yyvsp[0].chr; ;
    break;}
case 62:
#line 446 "micasmY.y"
{ yyval.chr = yyvsp[0].chr; ;
    break;}
case 63:
#line 449 "micasmY.y"
{ yyval.chr = yyvsp[0].chr; ;
    break;}
}

#line 709 "/usr/share/bison/bison.simple"


  yyvsp -= yylen;
  yyssp -= yylen;
#if YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;
#if YYLSP_NEEDED
  *++yylsp = yyloc;
#endif

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("parse error, unexpected ") + 1;
	  yysize += yystrlen (yytname[YYTRANSLATE (yychar)]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "parse error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[YYTRANSLATE (yychar)]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exhausted");
	}
      else
#endif /* defined (YYERROR_VERBOSE) */
	yyerror ("parse error");
    }
  goto yyerrlab1;


/*--------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action |
`--------------------------------------------------*/
yyerrlab1:
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;
      YYDPRINTF ((stderr, "Discarding token %d (%s).\n",
		  yychar, yytname[yychar1]));
      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;


/*-------------------------------------------------------------------.
| yyerrdefault -- current state does not do anything special for the |
| error token.                                                       |
`-------------------------------------------------------------------*/
yyerrdefault:
#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */

  /* If its default is to accept any token, ok.  Otherwise pop it.  */
  yyn = yydefact[yystate];
  if (yyn)
    goto yydefault;
#endif


/*---------------------------------------------------------------.
| yyerrpop -- pop the current state because it cannot handle the |
| error token                                                    |
`---------------------------------------------------------------*/
yyerrpop:
  if (yyssp == yyss)
    YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#if YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "Error: state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

/*--------------.
| yyerrhandle.  |
`--------------*/
yyerrhandle:
  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

/*---------------------------------------------.
| yyoverflowab -- parser overflow comes here.  |
`---------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}
#line 453 "micasmY.y"



#include "micasmL.lex.c"


int yyerror(char *s)
{
   error_line = yylineno;
   error_msg  = s;
   return(0);
}

int yywrap()
{
   error_line = 0;
   error_msg  = "";
   return(1);
}
