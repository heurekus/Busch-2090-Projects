/*
*   micasm.c
*/
#define VERSION "27.8.2005"
/*
*   assemble input file to a MIC file
*/
#define CPR "(c) 2005 Dr.-Ing. Ingo D. Rullhusen <d01c@uni-bremen.de>"
/*
* This program is free software; you can redistribute it and/or modify   
* it under the terms of the GNU General Public License as published by   
* the Free Software Foundation; either version 2 of the License, or   
* (at your option) any later version.   
* 
* This program is distributed in the hope that it will be useful,   
* but WITHOUT ANY WARRANTY; without even the implied warranty of   
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
* GNU General Public License for more details.   
*
* You should have received a copy of the GNU General Public License   
* along with this program; if not, write to the Free Software   
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*
*/


#include <stdio.h>


char ui2hex( unsigned char ui )
{
   switch( ui )
   {
      case 0x0: { return('0'); }
      case 0x1: { return('1'); }
      case 0x2: { return('2'); }
      case 0x3: { return('3'); }
      case 0x4: { return('4'); }
      case 0x5: { return('5'); }
      case 0x6: { return('6'); }
      case 0x7: { return('7'); }
      case 0x8: { return('8'); }
      case 0x9: { return('9'); }
      case 0xa: { return('A'); }
      case 0xb: { return('B'); }
      case 0xc: { return('C'); }
      case 0xd: { return('D'); }
      case 0xe: { return('E'); }
      case 0xf: { return('F'); }
   }
   return(' ');
}

unsigned char hex2ui( char hex )
{
   switch( hex )
   {
      case '0': { return(0x0); }
      case '1': { return(0x1); }
      case '2': { return(0x2); }
      case '3': { return(0x3); }
      case '4': { return(0x4); }
      case '5': { return(0x5); }
      case '6': { return(0x6); }
      case '7': { return(0x7); }
      case '8': { return(0x8); }
      case '9': { return(0x9); }
      case 'A':
      case 'a': { return(0xa); }
      case 'B':
      case 'b': { return(0xb); }
      case 'C':
      case 'c': { return(0xc); }
      case 'D':   
      case 'd': { return(0xd); }
      case 'E':
      case 'e': { return(0xe); }
      case 'F':
      case 'f': { return(0xf); }
   }
   return(0xff);
}


#include "micasmY.tab.c"


int main( int argc, char *argv[] )
{
   int i, j;
   FILE *micfile;

   if ( argc != 2 )
   {
      printf("micasm (version %s)\n",VERSION);
      printf(" Copyright %s\n",CPR);
      printf(" micasm comes with ABSOLUTELY NO WARRANTY; for details see GNU\n");
      printf(" GENERAL PUBLIC LICENSE. This is free software, and you are welcome to\n");
      printf(" redistribute it under certain conditions; see GNU GENERAL PUBLIC LICENSE\n");
      printf(" for details.\n\n");

      printf("micasm <.s file>\n");
      return(0);
   }


/* Y = 0 */
   filename = calloc( sizeof(char), strlen("a.mic")+1);
   strcpy( filename, "a.mic" );
   lno = 0;
   fill[0] = fill[1] = fill[2] = '0';
   for ( i=0; i<256; i++ ) label[i] = pre_label[i] = NULL;


/* asm */
   yyin = fopen( argv[1], "r" );
   if ( !yyin )
   {
      fprintf(stderr,"Cannot open file %s!\n",argv[1]);
      return(2);
   }
   
   yyout = fopen("/dev/null","w");

   yyparse();
   fclose(yyin);

   if ( error_line > 0 )
   {
      fprintf(stderr,"In line %i: %s\n",error_line,error_msg);
      return(5);
   }


   for ( i=lno; i<256; i++ )
   {
      memory[i][0] = fill[0];
      memory[i][1] = fill[1];
      memory[i][2] = fill[2];
   }

   for ( i=0; i<256; i++ )
   {
      if ( pre_label[i] )
      {
         for ( j=0; j<256; j++ )
         {
            if ( label[j] )
            {
               if ( strstr( label[j],     pre_label[i] ) &&
                    strstr( pre_label[i], label[j]     )    ) break;
            }
         }

         if ( j < 256 ) /* label found */
         {
            memory[i][1] = ui2hex( j/16 );
            memory[i][2] = ui2hex( j%16 );
         }
         else /* no label */
         {
            fprintf(stderr,"No reference for label %s!\n",pre_label[i]);
            return(5);
         }
      }
   }


/* out */
   micfile = fopen( filename, "w" );
   if ( ! micfile )
   {
      fprintf(stderr,"Cannot open file %s!\n",filename);
      return(2);
   }

   for ( i=0; i<256; i++ )
   {
      fprintf(micfile,"%c%c%c\r",memory[i][0],memory[i][1],memory[i][2]);
   }

   fclose(micfile);

   return(0);
}
