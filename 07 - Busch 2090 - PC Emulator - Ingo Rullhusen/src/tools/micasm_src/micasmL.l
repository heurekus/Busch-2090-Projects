/*
*   micasmL.l
*
*   version 27.8.2005
*   (c) 2005 Dr.-Ing. Ingo D. Rullhusen <d01c@uni-bremen.de>
*
* This program is free software; you can redistribute it and/or modify   
* it under the terms of the GNU General Public License as published by   
* the Free Software Foundation; either version 2 of the License, or   
* (at your option) any later version.   
* 
* This program is distributed in the hope that it will be useful,   
* but WITHOUT ANY WARRANTY; without even the implied warranty of   
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
* GNU General Public License for more details.   
*
* You should have received a copy of the GNU General Public License   
* along with this program; if not, write to the Free Software   
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*
*/


%option yylineno never-interactive yywrap


%{

#include <stdlib.h>

char *lbl = NULL;

%}


%%


[;].*  ; /* commend */
[ \t]+ ; /* ignore spaces and tabs */

[\n] { return(NEWL); }


[0]         { yylval.chr = yytext[0]; return(CHAR_0); }
[1-6]       { yylval.chr = yytext[0]; return(CHAR_1_6); }
[7-9A-Fa-f] { yylval.chr = (char)toupper(yytext[0]); return(CHAR_7_F); }

[,] { return(COMMA); }
[=] { return(EQ); }
[:] { return(COLON); }
[#] { return(NBR); }

["][^"]*["] { yylval.chstr = yytext; return(QUOTED); }
[_]([a-zA-Z0-9_]*) { if ( lbl ) free(lbl);
                     lbl = calloc( sizeof(char), yyleng+1 );
                     strcpy( lbl, yytext );
                     yylval.chstr = lbl;
                     return(LABEL);
                   }

[fF][iI][lL][lL] { return(FILL); }
[oO][uU][tT][pP][uU][tT] { return(OUTPUT); }

[mM][oO][vV][iI]  { return(MOVI); }
[mM][oO][vV][ \t] { return(MOV); }
[aA][nN][dD][iI]  { return(ANDI); }
[aA][nN][dD][ \t] { return(AND); }
[aA][dD][dD][iI]  { return(ADDI); }
[^#]?[aA][dD][dD][ \t] { return(ADD); }
[sS][uU][bB][iI]  { return(SUBI); }
[sS][uU][bB][ \t] { return(SUB); }
[cC][mM][pP][iI]  { return(CMPI); }
[cC][mM][pP][ \t] { return(CMP); }

[cC][aA][lL][lL] { return(CALL); }
[gG][oO][tT][oO] { return(GOTO); }
[bB][rR][cC]     { return(BRC); }
[bB][rR][zZ]     { return(BRZ); }

[hH][aA][lL][tT] { return(HALT); }
[nN][oO][pP]     { return(NOP); }
[dD][iI][sS][oO][uU][tT] { return(DISOUT); }
[hH][xX][dD][zZ] { return(HXDZ); }
[dD][zZ][hH][xX] { return(DZHX); }
[rR][nN][dD]     { return(RND); }
[tT][iI][mM][eE] { return(TIME); }
[rR][eE][tT]     { return(RET); }
[cC][lL][eE][aA][rR] { return(CLEAR); }
[sS][tT][cC]     { return(STC); }
[rR][sS][cC]     { return(RSC); }
[mM][uU][lL][tT] { return(MULT); }
[dD][iI][vV]     { return(DIV); }
[eE][xX][rR][lL] { return(EXRL); }
[eE][xX][rR][mM] { return(EXRM); }
[eE][xX][rR][aA] { return(EXRA); }
[dD][iI][sS][pP] { return(DISP); }

[mM][aA][sS]      { return(MAS); }
[iI][nN][vV]      { return(INV); }
[sS][hH][rR]      { return(SHR); }
[sS][hH][lL]      { return(SHL); }
[^#]?[aA][dD][cC] { return(ADC); }
[sS][uU][bB][cC]  { return(SUBC); }
[dD][iI][nN]      { return(DIN); }
[dD][oO][tT]      { return(DOT); }
[kK][iI][nN]      { return(KIN); }
