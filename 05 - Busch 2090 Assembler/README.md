# Busch-2090-Assembler
This directory contains a Busch 2090 cross-platform assembler that converts Busch 2090 assembly code into hexadecimal machine code and resolves symbolic destination addresses.

![Assembler Screenshot](https://github.com/martinsauter/Busch-2090-Tape-Emulator/blob/master/05%20-%20Busch%202090%20Assembler/assembler-screenshot.png)

### Motivation

The Busch 2090 Tape Emulator can download Busch 2090 machine code instructions from text files. The text files can be commented, the tape emulator will filter out the comments and only download the hexadecimal machine instructions. Modifying existing program files is not very convenient, however, because:

 * Assembly instructions (mnemonics) have to be converted to machine instructions (in hexadecimal) by hand.

 * Adding instructions in the middle of the code requires memory addresses for the remainder of the program to be updated manually.
 
 * Branch destination addresses change when code is inserted or deleted and have to be manually changed in branch instructions. This is a tedious, tiresome and error prone process when done manually because commented machine code files do usually not contain the memory locations of the instructions and destinations.

### Overview

A traditional assembler takes a text file with assembly mnemonics and symbolic branch destination addresses and creates a binary file containing the machine instructions. When a debugger is available it also creates a file with information to link the symbolic branch destinations to memory addresses. 

This approach does not work well for the Busch 2090 because the assembler runs on a PC while the code runs on the Busch 2090 where no debugger is available. The Busch 2090, however, allows to set breakpoints at memory addresses, single step through code and inspect registers. It would therefore be good to know the memory address of each instruction and have them side by side with the assembly instructions.

To accommodate for the different usage scenarios this assembler implementation takes a somewhat unconventional approach: The Busch 2090 program is written in a Libreoffice Calc document. The assembler is contained in the Calc document and was written in Libreoffice Basic. The source code in this directory is a copy/paste from the macro section of the Calc document so it can be conveniently examined on Github. Otherwise it has no function!


### Usage

 * Open the Libreoffice Calc file in this directory, it already contains an example Busch 2090 program in addition to the Assembler macro.
 
 * You will most likely get a security warning that the document contains macros which are blocked from executing. Adapt Libreoffice Macro security settings, e.g. by allowing Libreoffice files in this directory to execute macros. Close/reopen Libreoffice to activate the changes.
 
 * In the document column 1 (memory address) and column 2 (machine code) are empty. Column 3 holds the symbolic branch destination addresses. Column 4 contains the Busch 2090 assembly mnemonics with 0, 1 or 2 parameters.
 
 * Click on the "Assemble" button. The assembler will run and fill out column 1 and column 2. Modified cells are marked in green if everything is o.k. If there was a problem the corresponding cell is marked in red. Machine code translations which have been converted successfully but not yet been verified on a real 2090 hardware are colored in celestial.
 
 
### Instruction Code and Parameter conventions

 * The Mnemonic names are as per the Busch 2090 instruction manual
 
 * Register numbers are preprended with an r. Example: register 5 is r5
 
 * Immediate parameter values are prepended with a #. Example: The value a is represented as #A
 
 * If the instruction takes two parameters, they are separated by a comma. There must be no space after the comma.
 
 * After the assembly process has completed and all machine code cells are green or celestial, column 2 can be selected in Calc and copied/pasted into a text file. This text file can then be directly downloaded to the Busch 2090 with the Raspberry Pi tape emulator.
 
 * The Calc file with the 2090 program can then be used to determine at which memory locations to set break points on the 2090 hardware, perform single step execution and display register contents.
 
 * After modifying a program, click on the "Assemble" button again to update the memory addresses, machine code instructions and branch destinations.
 

