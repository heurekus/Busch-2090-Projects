REM  *****  BASIC  *****

'##############################################################################
'
' @version   1.4 2017-07-02
' @copyright Copyright (c) 2017 Martin Sauter, martin.sauter@wirelessmoves.com
' @license   GNU General Public License v2
' @since     Since Release 1.0
' 
'
' Overview
' #########
'
' A Busch 2090 Assembler written in Libreoffice basic that assembles 
' mnemonics for the Busch 2090 pseudo CPU to pseudo-machine language.
' The program to be assembled is programmed in Libreoffice Calc. 
' The assembly process works in two passes:
'
' 1. In the first pass the assembler analyzes which cells in column 3
'    (1st column = 0) contains an instruction code. For these lines the 
'    memory address is calculated and put in column 0. In addition, the
'    first pass creates a table of all symbolic jump destinations
'
' 2. In the second pass all instructions are assembled to machine code
'    and the result is put in column 1. For jumps, goto and brances, 
'    symbolic destinations are replaced by the destination memory address.
'
'
' Column one with the machine instructions can then be selected to be
' copied/pasted to a text file (e.g. example-program.mic) which can then
' be used with the Busch 2090 Load from Tape emulator program to be 
' transfered to the Busch 2090 hardware.
'
' For further details see the README.md file in the same directory.
'
' Version history
' #################
'
' v1.0, 19.6.2017: Initial release
' v1.1, 21.6.2017: Button to start assembly process added
'                  Clear memory and machine code fields before and assembly run
'                  Find last mnemonic and use this as upper bound for assembly
'                  loops.
' v1.2, 21.6.2017: Remaining mnemonics added. All are present now but not all
'                  are verified yet.
' v1.3, 24.6.2017: #defines for register names and constants introduced.
' v1.4, 02.7.2017: Additional instructions validated: NOP, SHR, SHL, RND, INV
'
'##############################################################################

'All variables must be declared!
Option Explicit

' Constants for cell colors
Const GREEN       = 1
Const CELESTIAL   = 2

Const COL_MEM           = 0
Const COL_MACHINE_CODE  = 1
Const COL_BRANCH_DEFINE = 2
Const COL_MNEMONIC      = 3

Sub Main

  'Variables for the Calc Sheet
  Dim Doc as Object
  Dim Sheet as Object
  Dim SheetNum as Integer
  Dim Cell as Object
  Dim CellContent as String
  
  Dim CellContentMnemonic as String
  Dim CellContentBranchDefine as String
    
  Doc = ThisComponent
  SheetNum = 0  
  Sheet = Doc.Sheets(SheetNum)

  'Loop variables
  Dim x as Integer 
  Dim start_line as Integer
  Dim end_line as Integer 
  Dim num_empty_lines as Integer
  Dim max_line as Integer
  start_line = 1
  end_line = 1
  num_empty_lines = 0
  max_line = 1024


  'Variables required to analyzed the Mnemonic line
  Dim Mnemonic as String
  Dim MnemonicEnds  as Integer
  
  Dim Parameters as String
  
  'The following variable keeps track of the memory addresses assigned
  'to instructions.
  Dim mem_address
  mem_address = 0

  'In the first pass all branch targets are put into an array
  Dim branch_array(255, 1) as String  
  Dim branch_index as Integer
  branch_index = 0
  
  'In the first pass all definitions are put into an array
  Dim define_array(255,1) as String
  Dim define_index as Integer
  define_index = 0 
  
  'Delete memory addresses, machine code and coloring of the fields
  'of the previous run before starting the assembly process.
  'In addition find out which line contains the mnemonic and use
  'this the end of the assembly loops that follow.
  For x = start_line to max_line
  
     'Check if the current line contains a mnemonic. If not
     'increase the empty line counter
     if Sheet.getCellByPosition(COL_MNEMONIC,x).String = "" then
       num_empty_lines = num_empty_lines + 1
     else
       num_empty_lines = 0
     end if
     
     'If there are no more mnemonics
     if num_empty_lines > 10 then
       exit for
     end if    
     
     'Delete memory address and machine code of this line
     Sheet.getCellByPosition(COL_MEM,x).String = ""
     Sheet.getCellByPosition(COL_MEM,x).CellBackColor = RGB(255,255,255)
     Sheet.getCellByPosition(COL_MACHINE_CODE,x).String = ""
     Sheet.getCellByPosition(COL_MACHINE_CODE,x).CellBackColor = RGB(255,255,255) 
  Next
  
  ' Set the loop counter for the loops below to the last command found
  end_line = x

  'First pass:
  '
  ' * Generate memory addresses
  ' * Generate branch destination table
  ' * Generate defintions translation table
  '
  '################################################################

  'Go through all lines in the first (0) sheet
  For x = start_line to end_line

    'Get content of the cell of the current line that contains the mnemonic
    Cell = Sheet.getCellByPosition(COL_MNEMONIC,x)
    CellContentMnemonic = Cell.String
    
    'Get contentof the cell of the current line that contains a branche name
    'or signals that the line contains a definition
    CellContentBranchDefine = trim(Sheet.getCellByPosition(COL_BRANCH_DEFINE,x).String)

    if CellContentBranchDefine = "#define" then

      'Line contains a defintion
      AddDefinition(CellContentMnemonic, define_array, define_index)

    elseif len(CellContentMnemonic) > 2 then   

      'Line contains a mnemonic

      Cell = Sheet.getCellByPosition(COL_MEM,x)
      Cell.String =  "0x" & ToHex(mem_address)
      Cell.CellBackColor = RGB(0,255,0)           

      if len (CellContentBranchDefine) > 2 then
      
        'A branch/call target name is in the line store the address in
        'the branch array            
      
        branch_array(branch_index,0) = CellContentBranchDefine
        branch_array(branch_index,1) = ToHex(mem_address)
        branch_index = branch_index + 1
      end if

      mem_address = mem_address + 1
        
    end if 'mnemonic handling
      
  Next

  'Second pass:
  '
  ' * Translate mnemonics to machine code
  '
  ' * Find GOTO, CALL, BRC, BRZ instructions and replace
  '   target symbols with real addresses from the 
  '   branch destination table array
  '
  '################################################################

  'Go through all lines in the first (0) sheet
  For x = start_line to end_line

    'Get the cell of the current line that contains the mnemonic
    Cell = Sheet.getCellByPosition(COL_MNEMONIC,x)
    CellContent = trim(Cell.String)
    
    'If a mnemonic is present in the line
    if len(CellContent) > 2 and _
       trim(Sheet.getCellByPosition(COL_BRANCH_DEFINE,x).String) <> "#define" then
        
        'Find the separator between the mnemonic and parameters
        MnemonicEnds = instr(CellContent, " ")
        
        'If parameters exist
        if MnemonicEnds > 0 then       
        
          Mnemonic = left(CellContent, MnemonicEnds - 1)
          Parameters = trim(right(CellContent, len(CellContent) - MnemonicEnds))
          ReplaceDefinitionsInParameters(Parameters, define_array, define_index)
         
        else ' a mnemonic without parameters
        
          Mnemonic = CellContent
          Parameters = ""
          
        end if
        
        TranslateMnemonicToMachineCode (Sheet, x, Mnemonic, Parameters, _
                                        branch_array, branch_index)
        
    end if 'mnemonic handling
      
  Next
  
  print "Done!"

End Sub


'##############################################################################
'
'Convert integer to string in hex format, prepend a 0 at the beginning for 
'single digit numbers
'
'##############################################################################

function ToHex (number_to_convert)

 Dim temp_str as string
 
 temp_str = hex(number_to_convert)
 if len(temp_str) < 2 then
   temp_str = "0" & temp_str
 end if
 ToHex = temp_str

end function 


'##############################################################################
'
' This subroutine is the base for translating mnemonics to machine code
'
'##############################################################################

sub TranslateMnemonicToMachineCode (Sheet, LineNum, Mnemonic, Parameters, _
                                    branch_array, branch_index)

  Dim Cell as Object
  Dim TargetAddress as String
  Dim MachineCode as String

  'Mnemonics with 2 parameters
  '##############################

  if Mnemonic = "MOV" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "0", Parameters, GREEN)
    exit sub      
  end if
   
  if Mnemonic = "MOVI" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "1", Parameters, GREEN)
    exit sub      
  end if
  
  if Mnemonic = "AND" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "2", Parameters, GREEN)
    exit sub      
  end if

  if Mnemonic = "ANDI" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "3", Parameters, GREEN)
    exit sub      
  end if 
  
  if Mnemonic = "ADD" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "4", Parameters, GREEN)
    exit sub      
  end if
  
  if Mnemonic = "ADDI" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "5", Parameters, GREEN)
    exit sub      
  end if
  
  if Mnemonic = "SUB" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "6", Parameters, GREEN)
    exit sub      
  end if  

  if Mnemonic = "SUBI" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "7", Parameters, GREEN)
    exit sub      
  end if

  if Mnemonic = "CMP" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "8", Parameters, GREEN)
    exit sub      
  end if

  if Mnemonic = "CMPI" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "9", Parameters, GREEN)
    exit sub      
  end if

  if Mnemonic = "OR" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "A", Parameters, CELESTIAL)
    exit sub      
  end if  

  'B, C, D, E are branch mnemonics and treated differently 
   
  if Mnemonic = "DISP" then
    WriteTwoParameterMachineCodeToCell (Sheet, LineNum, "F", Parameters, GREEN)
    exit sub      
  end if  


  'Mnemonics with 1 parameter
  '##############################

  if Mnemonic = "MAS" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "F7", Parameters, CELESTIAL)  
    exit sub      
  end if

  if Mnemonic = "INV" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "F8", Parameters, GREEN)
    exit sub      
  end if

  if Mnemonic = "SHR" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "F9", Parameters, GREEN)  
    exit sub      
  end if

  if Mnemonic = "SHL" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "FA", Parameters, GREEN)  
    exit sub      
  end if

  if Mnemonic = "ADC" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "FB", Parameters, GREEN)  
    exit sub      
  end if

  if Mnemonic = "SUBC" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "FC", Parameters, GREEN)  
    exit sub      
  end if

  if Mnemonic = "DIN" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "FD", Parameters, CELESTIAL)  
    exit sub      
  end if

  if Mnemonic = "DOT" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "FE", Parameters, GREEN)  
    exit sub      
  end if
  
  if Mnemonic = "KIN" then
    WriteSingleParameterMachineCodeToCell (Sheet, LineNum, "FF", Parameters, GREEN)
    exit sub      
  end if


  'Mnemonics without parameters
  '##############################
  
  if Mnemonic = "HALT" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F00"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)
    exit sub      
  end if
  
  if Mnemonic = "NOP" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F01"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)
    exit sub      
  end if

  if Mnemonic = "DISOUT" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F02"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)
    exit sub      
  end if

  if Mnemonic = "HXDZ" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F03"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)
    exit sub      
  end if

  if Mnemonic = "DZHX" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F04"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)
    exit sub      
  end if

  if Mnemonic = "RND" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F05"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)
    exit sub      
  end if
  
  if Mnemonic = "TIME" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F06"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255)
    exit sub      
  end if

  if Mnemonic = "RET" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F07"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)
    exit sub      
  end if

  if Mnemonic = "CLEAR" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F08"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)
    exit sub      
  end if
  
  if Mnemonic = "STC" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F09"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255)
    exit sub      
  end if
  
  if Mnemonic = "RSC" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F0A"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255)
    exit sub      
  end if  
  
  if Mnemonic = "MULT" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F0B"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255)
    exit sub      
  end if
  
  if Mnemonic = "DIV" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F0C"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255)
    exit sub      
  end if  
  
  if Mnemonic = "EXRL" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F0D"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255)
    exit sub      
  end if
  
  if Mnemonic = "EXRM" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F0E"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255)
    exit sub      
  end if
  
  if Mnemonic = "EXRA" then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "F0F"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255)
    exit sub      
  end if      


  'Branch mnemonics
  '##########################

  if Mnemonic = "CALL" then   
    TargetAddress = FindTargetAddress(Parameters, branch_array, branch_index)
    MachineCode = "B" & TargetAddress
    WriteBranchMachineCodeToCell (MachineCode, TargetAddress, Sheet, LineNum)
    exit sub
  end if
  
  if Mnemonic = "GOTO" then   
    TargetAddress = FindTargetAddress(Parameters, branch_array, branch_index)
    MachineCode = "C" & TargetAddress
    WriteBranchMachineCodeToCell (MachineCode, TargetAddress, Sheet, LineNum)
    exit sub
  end if
  
  if Mnemonic = "BRC" then   
    TargetAddress = FindTargetAddress(Parameters, branch_array, branch_index)
    MachineCode = "D" & TargetAddress
    WriteBranchMachineCodeToCell (MachineCode, TargetAddress, Sheet, LineNum)
    exit sub
  end if  
  
  if Mnemonic = "BRZ" then   
    TargetAddress = FindTargetAddress(Parameters, branch_array, branch_index)
    MachineCode = "E" & TargetAddress
    WriteBranchMachineCodeToCell (MachineCode, TargetAddress, Sheet, LineNum)
    exit sub
  end if    

  'If we end up here an unknown mnemonic was found
  Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "???"
  Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(255,0,0)

end sub


'##############################################################################
'
' Write a single parameter machine code translation to the cell
'
'##############################################################################

sub WriteSingleParameterMachineCodeToCell (Sheet, LineNum, Code, Parameter, Color)

  if len(Parameter) = 2 then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = Code & mid(Parameter,2,1)
    
    'Use different colors for machine instructions that are already
    'tested and those which have so far not been used.
    if Color = CELESTIAL then
      Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255) 'Celestial   
    else
      Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0) 'Green   
    end if

  else 'Parameter problem, can't translate
  
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "???"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(255,0,0) 'Red

  end if 

end sub


'##############################################################################
'
' Write a machine code translation to the cell that takes two parameters
'
'##############################################################################

sub WriteTwoParameterMachineCodeToCell (Sheet, LineNum, Code, Parameters, Color)

  if len(Parameters) = 5 then
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = _
      Code & mid(Parameters,2,1) & mid(Parameters,5,1)

    'Use different colors for machine instructions that are already
    'tested and those which have so far not been used.
    if Color = CELESTIAL then
      Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,255) 'Celestial   
    else
      Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0) 'Green   
    end if
    
  else'Parameter problem, can't translate
  
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = "???"
    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(255,0,0) 'Red 
    
  end if 

end sub

'##############################################################################
'
' Writes a branch machine code (CALL, BRC, BRZ, GOTO) with target address
' to the instruction cell
'
'##############################################################################

sub WriteBranchMachineCodeToCell (MachineCode, TargetAddress, Sheet, LineNum)

    Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).String = MachineCode
    if TargetAddress = "??" then
      Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(255,0,0)   
    else
      Sheet.getCellByPosition(COL_MACHINE_CODE,LineNum).CellBackColor = RGB(0,255,0)       
    end if

end sub


'##############################################################################
'
' This function goes through the brach_array and tries to find the target 
' address
'
'##############################################################################

function FindTargetAddress (Parameters, branch_array, branch_index)

  Dim x as Integer
  Dim TargetAddress as String
  
  TargetAddress = "??"
  
  for x = 0 to branch_index
    if branch_array(x,0) = Parameters then
       TargetAddress = branch_array(x,1)
      exit for
    end if
  next

  FindTargetAddress = TargetAddress
end function

'##############################################################################
'
' This subroutine analyzes the given definition string and if valid, adds it
' to the definition array. Example:
'
' cur_def = "OUT_DATA r0"
'
'##############################################################################


sub AddDefinition(cur_def, define_array, define_index) 

  Dim parts
  Dim upperBound as Integer
  
  'Split the definition into the name and content. 
  'Important: If there are several spaces between name and content
  'there are more than two parts!
  parts = Split(cur_def) 
  upperBound = ubound(parts)
  
  if upperBound < 1 then
    exit sub
  end if 

  'Add definition assignment to the array
  define_array(define_index, 0) = parts(0)  
  define_array(define_index, 1) = parts(upperBound) 
  define_index = define_index + 1

end sub


'##############################################################################
'
' This subroutine is called when an instruction has one or two parameters. 
' For each parameter the function looks if the parameter is a #define and if
' so replaces the definition content with the real value. Example:
'
'   #define NUM_STICKS_LEFT_LOW  r1
'   DISP #4,NUM_STICKS_LEFT_LOW
'
' is translated to:
'
'  DISP #4,r1
' 
' The subroutine is call be reference and the 'Parameters' input variable
' is modified directly.
'
'##############################################################################

sub ReplaceDefinitionsInParameters(Parameters, define_array, define_index)

  Dim x as Integer  
 
  Dim parts
  Dim upperBound as Integer
  
  parts = Split(Parameters, ",")
  upperBound = ubound(parts)

  'For single parameters make a 1:1 comparison of the full parameter string 
  if upperBound < 1 then

    'Search and replace the definition for the real content
    for x = 0 to define_index
      if Parameters = define_array(x,0) then
        Parameters = define_array(x,1)
        exit for
      end if  
    next
    
  else 'Mnemonic has 2 parameters

    'Search and replace the definition for the real content of the first parameter
    for x = 0 to define_index
      if parts(0) = define_array(x,0) then
        parts(0) = define_array(x,1)
        exit for
      end if  
    next

    'Search and replace the definition for the real content of the second parameter
    for x = 0 to define_index
      if parts(1) = define_array(x,0) then
        parts(1) = define_array(x,1)
        exit for
      end if  
    next
    
    Parameters = parts(0) & "," & parts(1)
    
  end if
   
end sub
